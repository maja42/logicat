package main

import (
	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
	"github.com/sirupsen/logrus"
)

type Domain interface {
	actor.Domain
	NetworkCount() int
	Clear()

	NetValue(netID uint64) logic.NetVal
}

// Networking is handling all network-related tasks of a schematic.
type Networking struct {
	interactionSys *nora.InteractionSystem
	schematic      *Schematic

	domain Domain

	wirePool *actor.WirePool
	bondPool *actor.BondPool

	pins     []*actor.Pin
	networks []*actor.Network
}

func NewNetworking(interactionSys *nora.InteractionSystem, schematic *Schematic, domain Domain) *Networking {
	a := &Networking{
		interactionSys: interactionSys,
		schematic:      schematic,
		domain:         domain,
		wirePool:       actor.NewWirePool(),
		bondPool:       actor.NewBondPool(),
	}
	return a
}

// Clear removes all existing networks.
func (a *Networking) Clear() {
	for _, net := range a.networks {
		net.Destroy()
	}
	a.networks = nil
	a.domain.Clear()
}

// RemoveAllPins removes all pins.
// This is called after all components have been removed from the schematic.
func (a *Networking) RemoveAllPins() {
	for _, pin := range a.pins {
		pin.Destroy()
	}
	a.pins = nil
}

func (a *Networking) Destroy() {
	assert.True(len(a.networks) == a.domain.NetworkCount(), "conflict: domain and networking contain different number of networks")

	for _, pin := range a.pins {
		pin.Destroy()
	}
	a.pins = nil
	for _, net := range a.networks {
		net.Destroy()
	}
	a.networks = nil
	a.domain.Clear()
	a.wirePool.Destroy()
}

func (a *Networking) AddPins(pins []*actor.Pin) {
	a.pins = append(a.pins, pins...)

	for _, pin := range pins {
		assert.True(pin.ConnectedNetwork() == 0, "the new pin should not be connected to a network")
	}
}

func (a *Networking) RemovePins(pins []*actor.Pin) {
	prevLen := len(a.pins)
	for _, pin := range pins {
		assert.True(pin.ConnectedNetwork() == 0, "the pin is still connected")
		for idx, p := range a.pins {
			if p == pin {
				a.pins = append(a.pins[:idx], a.pins[idx+1:]...)
				break
			}
		}
	}
	assert.True(len(a.pins)+len(pins) == prevLen, "Failed to find and remove all pins")
}

// CanConnect checks if a new wire could connect to the given position.
// It can not, if the given position already contains two or more networks that are incompatible (and cannot be merged).
func (a *Networking) CanConnect(pos vmath.Vec2i) bool {
	_, _, canConnect := a.bondingInfo(pos)
	return canConnect
}

// CanBond checks if all networks at a given position can be merged (bonded) together.
func (a *Networking) CanBond(pos vmath.Vec2i) bool {
	foundNetworks, canBondSingle, _ := a.bondingInfo(pos)
	return canBondSingle || foundNetworks > 1
}

// bondingInfo checks what happens if a bond is created at the given location.
// Returns:
//	- the number of different networks that would be bonded (merged) together
//	- if at least one network would be bonded to itself, creating a loop
//	- if a new segment can be connected at the given location without causing a conflict
// If the last return value is false, the other returns are (0, false) - bonding is not possible.
func (a *Networking) bondingInfo(pos vmath.Vec2i) (int, bool, bool) {
	networks := a.GetNetworks(pos)

	foundNetworks := 0
	canBondSingle := false // if a single network can be bonded to create a loop

	var driverPin *actor.Pin
	for _, net := range networks {
		if !net.Contains(pos) {
			continue
		}
		foundNetworks++

		// check output pin:
		out := net.OutputPin()
		if driverPin == nil {
			driverPin = out
		} else if out != nil && out != driverPin { // Cannot merge networks (different output pins)
			return 0, false, false
		}

		if !canBondSingle && net.CanBond(pos) {
			canBondSingle = true // the network can be bonded with itself to create a loop
		}
	}
	// NOTE: when forbidding loops, this needs to be checked here too!
	return foundNetworks, canBondSingle, true
}

// TryBond tries to merge (bond) all networks at a given position.
// Returns true if a new bond was created.
func (a *Networking) TryBond(pos vmath.Vec2i) bool {
	if !a.CanBond(pos) {
		return false
	}

	var firstNet *actor.Network
	var merged []*actor.Network
	for _, net := range a.networks {
		if !net.Contains(pos) {
			continue
		}

		if firstNet == nil {
			firstNet = net
			continue
		}
		firstNet.Merge(net)
		merged = append(merged, net)
	}
	assert.True(firstNet != nil, "canBond and merging was inconsistent")

	for _, net := range merged {
		a.removeNetwork(net)
		net.Destroy()
	}

	firstNet.Bond(pos)
	return true
}

func (a *Networking) AddWireSegment(segment utils.LineSegment2i, bonds *Bonds) *actor.Network {
	assert.True(segment.SquareLength() > 0, "wire segment too short")
	assert.False(bonds.HasConflict(), "bonds contain conflict")

	network := a.mergeNetworks(bonds.Networks)
	if network == nil {
		network = actor.NewNetwork(a.domain, a.wirePool, a.bondPool)
		a.networks = append(a.networks, network)
		assert.True(len(a.networks) == a.domain.NetworkCount(), "conflict: domain and networking contain different number of networks")
	}

	network.AddWireSegment(segment) // must happen before adding pins

	if bonds.OutputPin != nil {
		assert.True(network.OutputPin() == nil || network.OutputPin() == bonds.OutputPin, "conflicting output pin")
		network.SetOutputPin(bonds.OutputPin)
	}

	for inputPin := range bonds.InputPins {
		network.AddInputPin(inputPin)
	}

	logrus.Infof("added wire segment to network %s", network.Name())
	return network
}

func (a *Networking) mergeNetworks(networks map[*actor.Network]vmath.Vec2i) *actor.Network {
	var firstNet *actor.Network
	for net := range networks {
		if firstNet == nil {
			firstNet = net
			continue
		}
		firstNet.Merge(net)
		a.removeNetwork(net)
		net.Destroy()
	}
	return firstNet
}

func (a *Networking) removeNetwork(net *actor.Network) {
	for idx, n := range a.networks {
		if n == net {
			a.networks = append(a.networks[:idx], a.networks[idx+1:]...)
			return
		}
	}
}

func (a *Networking) GetPin(pos vmath.Vec2i) *actor.Pin {
	for _, pin := range a.pins {
		if pin.SchemaPos() == pos {
			return pin
		}
	}
	return nil
}

func (a *Networking) GetPins(pos []vmath.Vec2i) []*actor.Pin {
	var pins []*actor.Pin
	for _, p := range pos {
		if pin := a.GetPin(p); pin != nil {
			pins = append(pins, pin)
		}
	}
	return pins
}

func (a *Networking) Networks() []*actor.Network {
	return a.networks
}

// HasNetworks returns true if the given position contains a segment/wire.
func (a *Networking) HasNetworks(pos vmath.Vec2i) bool {
	for _, net := range a.networks {
		if net.Contains(pos) {
			return true
		}
	}
	return false
}

// GetNetworks returns all networks that cover the given position.
// If more than one network is returned, it means they are crossing but are not connected.
// If a single network touches the position multiple times, it is only returned once.
func (a *Networking) GetNetworks(pos vmath.Vec2i) []*actor.Network {
	var nets []*actor.Network

	for _, net := range a.networks {
		if net.Contains(pos) {
			nets = append(nets, net)
		}
	}
	return nets
}

func (a *Networking) StealSegment(pos vmath.Vec2i) *actor.StolenSegment {
	for _, net := range a.networks {
		if info := net.StealSegment(pos); info != nil {
			return info
		}
	}
	return nil
}

//// Segments returns the first n segments at the given position.
//// If max is 8 or higher, all segments are returned as there can't be more than 8 segments at a given location.
//func (a *Networking) Segments(pos vmath.Vec2i, max int) []utils.LineSegment2i {
//	var segments []utils.LineSegment2i
//
//	for _, net := range a.networks {
//		netSegs := net.Segments(pos, max)
//		max -= len(netSegs)
//		segments = append(segments, netSegs...)
//		if max <= 0 {
//			return segments
//		}
//	}
//	return segments
//}

func (a *Networking) NetworkCount() int {
	return len(a.networks)
}

func (a *Networking) Draw(renderState *nora.RenderState) {
	for _, net := range a.networks {
		net.Draw(renderState)
	}
}
