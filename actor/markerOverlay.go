package actor

import (
	"github.com/maja42/logicat/theme"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/builtin/geometry/geo2d"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

// MarkerOverlay is used to render debug information.
type MarkerOverlay struct {
	transform nora.Transform
	meshes    map[string]*nora.Mesh
}

func NewMarkerOverlay() *MarkerOverlay {
	a := &MarkerOverlay{
		meshes: make(map[string]*nora.Mesh),
	}
	a.transform.ClearTransform()
	return a
}

func (a *MarkerOverlay) ShowBonds(good, bad []vmath.Vec2i) {
	segments := 32
	a.markPositions("__goodBonds", theme.OuterHookRadius, theme.InnerHookRadius, segments, theme.GoodHookColor, good...)
	a.markPositions("__badBonds", theme.OuterHookRadius, theme.InnerHookRadius, segments, theme.BadHookColor, bad...)
}

func (a *MarkerOverlay) RemoveBonds() {
	a.RemoveMarkers("__goodBonds")
	a.RemoveMarkers("__badBonds")
}

// MarkPositions adds position markers
func (a *MarkerOverlay) MarkPositions(name string, color color.Color, positions ...vmath.Vec2i) {
	a.markPositions(name, 0.3, 0.1, 32, color, positions...)
}

// MarkPositionsf adds position markers
func (a *MarkerOverlay) MarkPositionsf(name string, color color.Color, positions ...vmath.Vec2f) {
	a.RemoveMarkers(name)
	if len(positions) == 0 {
		return
	}
	geometry := nora.Geometry{}
	for _, pos := range positions {
		geometry.AppendGeometry(geo2d.Ring(pos, 0.3, 0.1, 32))
	}
	a.addGeometry(name, &geometry, color)
}

func (a *MarkerOverlay) markPositions(name string, outerRadius, innerRadius float32, segments int, color color.Color, positions ...vmath.Vec2i) {
	a.RemoveMarkers(name)
	if len(positions) == 0 {
		return
	}
	geometry := nora.Geometry{}
	for _, pos := range positions {
		geometry.AppendGeometry(geo2d.Ring(pos.Vec2f(), outerRadius, innerRadius, segments))
	}
	a.addGeometry(name, &geometry, color)
}

func (a *MarkerOverlay) MarkVector(name string, color color.Color, pos vmath.Vec2f, vec vmath.Vec2f) {
	seg := utils.LineSegment2f{
		Start: pos,
		End:   pos.Add(vec),
	}
	leftArrowVec := vec.Rotate(vmath.ToRadians(-135)).Normalize()

	leftArrow := utils.LineSegment2f{
		Start: seg.End,
		End:   seg.End.Add(leftArrowVec),
	}

	rightArrowVec := vec.Rotate(vmath.ToRadians(135)).Normalize()
	rightArrow := utils.LineSegment2f{
		Start: seg.End,
		End:   seg.End.Add(rightArrowVec),
	}

	a.markLineSegment2f(name, 0.15, color, seg, leftArrow, rightArrow)
}

func (a *MarkerOverlay) MarkLineSegment2i(name string, color color.Color, segments ...utils.LineSegment2i) {
	segf := make([]utils.LineSegment2f, len(segments))
	for i, seg := range segments {
		segf[i] = seg.LineSegment2f()
	}
	a.MarkLineSegment2f(name, color, segf...)
}

func (a *MarkerOverlay) MarkLineSegment2f(name string, color color.Color, segments ...utils.LineSegment2f) {
	a.markLineSegment2f(name, theme.WireWidth/3, color, segments...)
}

func (a *MarkerOverlay) markLineSegment2f(name string, thickness float32, color color.Color, segments ...utils.LineSegment2f) {
	mat := nora.NewMaterial(shader.COL_2D)
	mat.Uniform4fColor("fragColor", color)
	mesh := nora.NewMesh(mat)

	geometry := &nora.Geometry{}
	for _, seg := range segments {
		points := []vmath.Vec2f{seg.Start, seg.End}
		geometry.AppendGeometry(geo2d.LineStrip(points, false, thickness, geo2d.MitterJoint, geo2d.FlatLineCap, geo2d.FlatLineCap))
	}

	mesh.SetGeometry(geometry)
	a.addMesh(name, mesh)
}

// MarkPinList adds position markers to all pins
func (a *MarkerOverlay) MarkPinList(name string, radius float32, color color.Color, pins PinList) {
	a.RemoveMarkers(name)
	if len(pins) == 0 {
		return
	}
	geometry := nora.Geometry{}
	for pin := range pins {
		geometry.AppendGeometry(geo2d.Ring(pin.SchemaPos().Vec2f(), radius, radius*0.9, 16))
	}
	a.addGeometry(name, &geometry, color)
}

func (a *MarkerOverlay) addGeometry(name string, geom *nora.Geometry, color color.Color) {
	mat := nora.NewMaterial(shader.COL_2D)
	mat.Uniform4fColor("fragColor", color)
	mesh := nora.NewMesh(mat)
	mesh.SetGeometry(geom)
	a.addMesh(name, mesh)
}

func (a *MarkerOverlay) addMesh(name string, mesh *nora.Mesh) {
	if old, ok := a.meshes[name]; ok {
		old.Destroy()
	}
	a.meshes[name] = mesh
}

func (a *MarkerOverlay) RemoveMarkers(name string) {
	if m, ok := a.meshes[name]; ok {
		m.Destroy()
		delete(a.meshes, name)
	}
}

func (a *MarkerOverlay) Clear() {
	for _, m := range a.meshes {
		m.Destroy()
	}
	a.meshes = make(map[string]*nora.Mesh)
}

func (a *MarkerOverlay) Destroy() {
	for _, m := range a.meshes {
		m.Destroy()
	}
	a.meshes = nil
}

func (a *MarkerOverlay) Draw(renderState *nora.RenderState) {
	for _, m := range a.meshes {
		m.Draw(renderState)
	}
}
