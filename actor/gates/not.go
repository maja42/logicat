package gates

import (
	"fmt"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/vmath"
)

type NotGate struct {
	baseGate
}

func NewNotGate(pinCount int) *NotGate {
	gate := &NotGate{
		baseGate: *newBaseGate("1", pinCount),
	}

	// inputs:
	for i := 0; i < pinCount; i++ {
		pinPos := vmath.Vec2i{0, -i}
		gate.pins = append(gate.pins, actor.NewInputPin(pinPos, geom.Left, geom.NoMark))
	}
	// output:
	for i := 0; i < pinCount; i++ {
		pinPos := vmath.Vec2i{gateWidth, -i}
		inputPin := i
		gate.pins = append(gate.pins, actor.NewOutputPin(pinPos, geom.Right, geom.CircleMark, func(netQuery logic.NetValueQueryFunc) logic.NetVal {
			net := gate.pins[inputPin].ConnectedNetwork()
			return (^netQuery(net)) & 0x0F // TODO: get the network's bitWidth!
		}))
	}
	return gate
}

func (a *NotGate) String() string {
	return fmt.Sprintf("NOT(%d)", len(a.pins)-1)
}
