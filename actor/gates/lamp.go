package gates

import (
	"fmt"
	"time"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/logicat/theme"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/builtin/shapes"
	"github.com/maja42/vmath"
	"github.com/maja42/vmath/math32"
)

const lampHeight = float32(1.5)
const textScale = float32(1.15)

type Lamp struct {
	netQuery logic.NetValueQueryFunc

	transform nora.Transform
	mesh      nora.Mesh

	bitWidth int

	value      logic.NetVal
	displayFmt logic.DisplayFormat
	text       shapes.Text

	position    vmath.Vec2i
	localBounds vmath.Rectf
	pin         *actor.Pin
}

// LocalBounds return the component's bounding box in model-space.
func (a *Lamp) LocalBounds() vmath.Rectf {
	return a.localBounds
}

// GlobalBounds return the gate's bounding box on the schematic.
func (a *Lamp) GlobalBounds() vmath.Rectf {
	return a.LocalBounds().Add(a.position.Vec2f())
}

// Position returns the component's (global) position on the schematic.
func (a *Lamp) Position() vmath.Vec2i {
	return a.position
}

func NewLamp(bitWidth int, displayFmt logic.DisplayFormat, netQuery logic.NetValueQueryFunc) *Lamp {
	assert.True(theme.LampValueTextFont.Monospace, "lamp expects monospace font")

	mat := nora.NewMaterial(shader.RGB_2D)

	a := &Lamp{
		netQuery:   netQuery,
		mesh:       *nora.NewMesh(mat),
		bitWidth:   bitWidth,
		displayFmt: displayFmt,
	}
	a.transform.ClearTransform()

	valTxt := a.value.String(a.bitWidth, a.displayFmt)
	text := shapes.NewText(theme.LampValueTextFont, valTxt)
	text.SetColor(theme.GateLabelColor)
	text.UniformScale(textScale)
	textBounds := text.Bounds()
	textSize := textBounds.Size().MulScalar(textScale)

	width := textSize[0] + 1
	width = math32.Ceil(width*2) / 2 // Round to 0.5

	textPos := vmath.Vec2f{
		theme.PinLength + (width-textSize[0])/2 - textBounds.Min[0]*textScale,
		-(textSize[1] / 2) - textBounds.Min[1]*textScale,
	}
	text.SetPositionXY(textPos[0], textPos[1])

	a.text = *text

	a.localBounds = vmath.Rectf{
		Min: vmath.Vec2f{theme.PinLength, -lampHeight / 2},
		Max: vmath.Vec2f{theme.PinLength + width, lampHeight / 2},
	}

	body := geom.BorderedRect(a.localBounds, theme.BodyBorderWidth, theme.BodyColor, theme.BodyBorderColor)
	a.mesh.SetGeometry(body)

	a.pin = actor.NewInputPin(vmath.Vec2i{0, 0}, geom.Left, geom.NoMark)
	return a
}

func (a *Lamp) SetPosition(pos vmath.Vec2i) {
	a.position = pos
	a.transform.SetPositionXY(float32(pos[0]), float32(pos[1]))
	a.pin.SetParentPos(pos)
}

// Pins returns all component pins where wires can be connected in model-space.
func (a *Lamp) Pins() []*actor.Pin {
	return []*actor.Pin{a.pin}
}

func (a *Lamp) String() string {
	return fmt.Sprintf("LAMP(%d)", a.bitWidth)
}

func (a *Lamp) Destroy() {
	a.mesh.Destroy()
	a.pin.Destroy()
}

func (a *Lamp) Update(elapsed time.Duration) {
	val := a.netQuery(a.pin.ConnectedNetwork())
	if a.value == val {
		return
	}
	a.value = val
	a.text.Set(a.value.String(a.bitWidth, a.displayFmt))
}

func (a *Lamp) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.PushMulRight(a.transform.GetTransform())
	a.mesh.Draw(renderState)
	a.text.Draw(renderState)
	a.pin.Draw(renderState)
	renderState.TransformStack.Pop()
}
