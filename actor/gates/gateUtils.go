package gates

import (
	"github.com/maja42/logicat/theme"
	"github.com/maja42/nora/builtin/shapes"
	"github.com/maja42/vmath"
	"github.com/maja42/vmath/math32"
)

// gateLabel returns a centered text to be used as a gate label
func gateLabel(label string, space vmath.Rectf) *shapes.Text {
	text := shapes.NewText(theme.GateLabelFont, label)
	text.SetColor(theme.GateLabelColor)

	spaceSize := space.Size()
	labelBounds := text.Bounds()
	labelSize := labelBounds.Size()

	// determine scaling
	maxScale := spaceSize.Div(labelSize)
	scale := math32.Min(maxScale[0]*0.75, maxScale[1]) // never consume the whole width
	scale = math32.Min(scale, theme.GateLabelMaxSize)
	// apply scaling
	labelSize = labelSize.MulScalar(scale)
	text.UniformScale(scale)
	// determine position for centering
	pos := space.Min.
		Add(spaceSize.Sub(labelSize).DivScalar(2)).
		Sub(labelBounds.Min.MulScalar(scale))
	// apply position
	text.SetPositionXY(pos[0], pos[1])
	return text
}
