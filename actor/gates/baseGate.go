package gates

import (
	"fmt"
	"time"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/theme"
	"github.com/maja42/nora"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/builtin/shapes"
	"github.com/maja42/vmath"
)

//type GateStatus int
//
//const (
//	GatePassive = GateStatus(iota)
//	GateConflict
//)

const (
	gateWidth = int(5)
)

type baseGate struct {
	transform nora.Transform
	mesh      nora.Mesh
	label     shapes.Text

	position    vmath.Vec2i
	localBounds vmath.Rectf
	pins        []*actor.Pin
}

// LocalBounds return the component's bounding box in model-space.
func (a *baseGate) LocalBounds() vmath.Rectf {
	return a.localBounds
}

// GlobalBounds return the gate's bounding box on the schematic.
func (a *baseGate) GlobalBounds() vmath.Rectf {
	return a.LocalBounds().Add(a.position.Vec2f())
}

// Position returns the gate's position on the schematic.
func (a *baseGate) Position() vmath.Vec2i {
	return a.position
}

func newBaseGate(label string, verticalPins int) *baseGate {
	mat := nora.NewMaterial(shader.RGB_2D)

	a := &baseGate{
		mesh: *nora.NewMesh(mat),
	}
	a.transform.ClearTransform()

	extraSpace := float32(0.5) // space between the first/last pin and the end of the body

	a.localBounds = vmath.RectfFromCorners(vmath.Vec2f{0, float32(-verticalPins) - extraSpace}, vmath.Vec2f{float32(gateWidth), 1 + extraSpace})
	a.localBounds.Min = a.localBounds.Min.AddScalar(theme.PinLength)
	a.localBounds.Max = a.localBounds.Max.SubScalar(theme.PinLength)

	body := geom.BorderedRect(a.localBounds, theme.BodyBorderWidth, theme.BodyColor, theme.BodyBorderColor)
	a.mesh.SetGeometry(body)

	a.label = *gateLabel(label, a.localBounds)
	return a
}

func (a *baseGate) SetPosition(pos vmath.Vec2i) {
	a.position = pos
	a.transform.SetPositionXY(float32(pos[0]), float32(pos[1]))
	for _, pin := range a.pins {
		pin.SetParentPos(pos)
	}
}

// Pins returns all pins of this component.
func (a *baseGate) Pins() []*actor.Pin {
	return a.pins
}

func (a *baseGate) String() string {
	return fmt.Sprintf("Gate(%d)", len(a.pins)-1)
}

func (a *baseGate) Destroy() {
	a.mesh.Destroy()
	a.label.Destroy()
	for _, pin := range a.pins {
		pin.Destroy()
	}
}

func (a *baseGate) Update(elapsed time.Duration) {
}

func (a *baseGate) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.Push()
	renderState.TransformStack.MulRight(a.transform.GetTransform())
	a.mesh.Draw(renderState)
	a.label.Draw(renderState)
	for _, pin := range a.pins {
		pin.Draw(renderState)
	}
	renderState.TransformStack.Pop()
}
