package gates

import (
	"fmt"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/vmath"
)

type OrGate struct {
	baseGate
}

func NewOrGate(inputPinCount int) *OrGate {
	gate := &OrGate{
		baseGate: *newBaseGate("≥1", inputPinCount),
	}

	// inputs:
	for i := 0; i < inputPinCount; i++ {
		pinPos := vmath.Vec2i{0, -i}
		gate.pins = append(gate.pins, actor.NewInputPin(pinPos, geom.Left, geom.NoMark))
	}
	// output:
	pinPos := vmath.Vec2i{gateWidth, 0}
	gate.pins = append(gate.pins, actor.NewOutputPin(pinPos, geom.Right, geom.NoMark, func(netQuery logic.NetValueQueryFunc) logic.NetVal {
		var val logic.NetVal
		for i := 0; i < inputPinCount; i++ {
			val |= netQuery(gate.pins[i].ConnectedNetwork())
		}
		return val
	}))
	return gate
}

func (a *OrGate) String() string {
	return fmt.Sprintf("OR(%d)", len(a.pins)-1)
}
