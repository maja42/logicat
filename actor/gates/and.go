package gates

import (
	"fmt"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/vmath"
)

type AndGate struct {
	baseGate
}

func NewAndGate(inputPinCount int) *AndGate {
	gate := &AndGate{
		baseGate: *newBaseGate("&", inputPinCount),
	}

	// inputs:
	for i := 0; i < inputPinCount; i++ {
		pinPos := vmath.Vec2i{0, -i}
		gate.pins = append(gate.pins, actor.NewInputPin(pinPos, geom.Left, geom.NoMark))
	}
	// output:
	pinPos := vmath.Vec2i{gateWidth, 0}
	gate.pins = append(gate.pins, actor.NewOutputPin(pinPos, geom.Right, geom.NoMark, func(netQuery logic.NetValueQueryFunc) logic.NetVal {
		var val = logic.MaxNetVal
		for i := 0; i < inputPinCount; i++ {
			val &= netQuery(gate.pins[i].ConnectedNetwork())
		}
		return val
	}))
	return gate
}

func (a *AndGate) String() string {
	return fmt.Sprintf("AND(%d)", len(a.pins)-1)
}
