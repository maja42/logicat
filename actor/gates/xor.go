package gates

import (
	"fmt"
)

type XorGate struct {
	baseGate
}

func NewXorGate(inputPinCount int) *XorGate {
	return &XorGate{
		baseGate: *newBaseGate("=1", inputPinCount),
	}
}

func (a *XorGate) String() string {
	return fmt.Sprintf("XOR(%d)", len(a.pins)-1)
}
