package gates

import (
	"fmt"
	"time"

	"github.com/maja42/glfw"
	"github.com/maja42/logicat/action"
	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/logicat/theme"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
	"github.com/sirupsen/logrus"
)

const buttonSize = float32(1.5)

type Button struct {
	transform nora.Transform
	mesh      nora.Mesh

	mpanel *action.Panel

	bitWidth  int
	clickable []*action.Clickable

	input logic.NetVal

	position    vmath.Vec2i
	localBounds vmath.Rectf
	pin         *actor.Pin
}

// LocalBounds return the component's bounding box in model-space.
func (a *Button) LocalBounds() vmath.Rectf {
	return a.localBounds
}

// GlobalBounds return the gate's bounding box on the schematic.
func (a *Button) GlobalBounds() vmath.Rectf {
	return a.LocalBounds().Add(a.position.Vec2f())
}

// Position returns the component's (global) position on the schematic.
func (a *Button) Position() vmath.Vec2i {
	return a.position
}

func NewButton(mpanel *action.Panel, bitWidth int) *Button {
	assert.True(bitWidth > 0 && bitWidth <= logic.MaxNetSize, "illegal bit width")

	mat := nora.NewMaterial(shader.RGB_2D)
	a := &Button{
		mesh:      *nora.NewMesh(mat),
		mpanel:    mpanel,
		bitWidth:  bitWidth,
		clickable: make([]*action.Clickable, bitWidth),
	}
	a.transform.ClearTransform()

	a.localBounds.SetSize(vmath.Vec2f{
		float32(bitWidth) * buttonSize,
		buttonSize,
	})
	a.localBounds.SetPos(vmath.Vec2f{
		-a.localBounds.Right() - theme.PinLength,
		buttonSize / 2,
	})

	a.updateGeometry()

	for i := 0; i < bitWidth; i++ {
		bit := i
		a.clickable[i] = mpanel.NewClickable(a.buttonRect(bit), glfw.MouseButtonLeft, func(mods glfw.ModifierKey) {
			a.input.ToggleBit(bit)
			logrus.Debugf("Changed button value to 0x%x", a.input)
			a.updateGeometry()
		})
	}

	a.pin = actor.NewOutputPin(vmath.Vec2i{0, 0}, geom.Right, geom.NoMark, func(_ logic.NetValueQueryFunc) logic.NetVal {
		return a.input
	})

	return a
}

func (a *Button) updateGeometry() {
	body := &nora.Geometry{}
	for bit := 0; bit < a.bitWidth; bit++ {
		buttonRect := a.buttonRect(bit)
		bodyColor := theme.BodyColor
		if a.input.Bit(bit) {
			bodyColor = color.DarkGreen
		}

		g := geom.BorderedRect(buttonRect, theme.BodyBorderWidth, bodyColor, theme.BodyBorderColor)
		body.AppendGeometry(g)
	}
	a.mesh.SetGeometry(body)
}

func (a *Button) SetPosition(pos vmath.Vec2i) {
	a.position = pos
	a.transform.SetPositionXY(float32(pos[0]), float32(pos[1]))
	a.pin.SetParentPos(pos)

	for bit, clickable := range a.clickable {
		localRect := a.buttonRect(bit)
		schemaRect := localRect.Add(pos.Vec2f())
		clickable.Move(schemaRect)
	}
}

func (a *Button) buttonRect(bit int) vmath.Rectf {
	assert.True(bit >= 0 && bit < a.bitWidth, "invalid bit %d", bit)

	offset := vmath.Vec2f{float32(bit)*-buttonSize - theme.PinLength, 0}
	pos := offset
	return vmath.Rectf{
		Min: pos.Add(vmath.Vec2f{-buttonSize, -buttonSize / 2}),
		Max: pos.Add(vmath.Vec2f{0, buttonSize / 2}),
	}
}

// Pins returns all component pins where wires can be connected in model-space.
func (a *Button) Pins() []*actor.Pin {
	return []*actor.Pin{a.pin}
}

func (a *Button) String() string {
	return fmt.Sprintf("BUTTON(%d)", a.bitWidth)
}

func (a *Button) Destroy() {
	for _, c := range a.clickable {
		c.Destroy()
	}
	a.mesh.Destroy()
	a.pin.Destroy()
}

func (a *Button) Update(elapsed time.Duration) {
}

func (a *Button) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.PushMulRight(a.transform.GetTransform())
	a.mesh.Draw(renderState)
	a.pin.Draw(renderState)
	renderState.TransformStack.Pop()
}
