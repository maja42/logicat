package actor

import (
	"github.com/maja42/logicat/utils/pool"
	"github.com/maja42/nora/assert"
)

const initialWirePoolSize = 50
const maxWirePoolSize = 200

// WirePool is a pool that supplies objects of type "*Wire".
// Unneeded objects should be returned to the pool.
// Retrieved objects are reset and therefore clean.
type WirePool struct {
	pool pool.Pool
}

func NewWirePool() *WirePool {
	newFunc := func() pool.Poolable {
		return NewWire()
	}
	resetFunc := func(wire pool.Poolable) {
		wire.(*Wire).Reset()
	}
	return &WirePool{
		pool: *pool.NewPool(initialWirePoolSize, maxWirePoolSize, newFunc, resetFunc),
	}
}

func (p *WirePool) Get() *Wire {
	return p.pool.Get().(*Wire)
}

func (p *WirePool) Put(wires ...*Wire) {
	for _, b := range wires {
		assert.True(b != nil, "cannot return nil to pool")
		p.pool.Put(b)
	}
}

func (p *WirePool) Destroy() {
	p.pool.Destroy()
}
