package actor

import (
	"github.com/maja42/logicat/theme"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/shapes"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

type Wire struct {
	shape shapes.LineStrip2D
}

func NewWire() *Wire {
	shape := shapes.NewLineStrip2D(theme.WireWidth, theme.WireLineJoint, false)
	shape.SetLineCaps(theme.WireLineCap, theme.WireLineCap)
	w := &Wire{
		shape: *shape,
	}
	return w
}

func (w *Wire) SetColor(color color.Color) {
	w.shape.SetColor(color)
	//w.shape.SetColor(utils.RandomColor())
}

//func (w *Wire) SetStatus(status WireStatus) {
//	w.status = status
//	switch status {
//	case WireDisconnected:
//		w.shape.SetColor(theme.WirePassiveColor)
//	case WireInactive:
//		w.shape.SetColor(theme.WireInactiveColor)
//	case WireActive:
//		w.shape.SetColor(theme.WireActiveColor)
//	case WirePlacement:
//		w.shape.SetColor(theme.WirePlacementColor)
//	case WireConflict:
//		w.shape.SetColor(theme.WireConflictColor)
//	}
//}

func (w *Wire) AddPoints(points ...vmath.Vec2i) {
	for _, p := range points {
		w.AddPoint(p)
	}
}

func (w *Wire) AddPoint(point vmath.Vec2i) {
	length := w.shape.Length()
	if length < 2 {
		w.shape.AddPoints(point.Vec2f())
		return
	}
	prev1 := w.shape.Point(length - 1).Vec2i()
	prev2 := w.shape.Point(length - 2).Vec2i()
	lastVec := prev2.Sub(prev1)
	pointVec := point.Sub(prev1)

	if lastVec.IsParallel(pointVec) {
		// move point (extend existing line segment)
		w.shape.SetPoint(length-1, point.Vec2f())
	} else {
		w.shape.AddPoints(point.Vec2f())
	}
}

func (w *Wire) endVec() vmath.Vec2i {
	length := w.shape.Length()
	assert.True(length >= 2, "wire should not exist (too short: %d)", length)

	vec := w.shape.Point(length - 1).Sub(w.shape.Point(length - 2))
	return vec.Vec2i()
}

func (w *Wire) AddPointsAtFront(points ...vmath.Vec2i) {
	existing := w.shape.Points()
	w.shape.ClearPoints()
	w.AddPoints(points...)
	if len(existing) >= 2 {
		// Add first points via "wire.AddPoint()" to ensure segments are correctly merged
		w.AddPoint(existing[0].Vec2i())
		w.AddPoint(existing[1].Vec2i())
		existing = existing[2:]
	}
	w.shape.AddPoints(existing...)
}

func (w *Wire) AddPointAtFront(point vmath.Vec2i) {
	w.AddPointsAtFront(point)
}

func (w *Wire) RemoveLastPoint() bool {
	return w.shape.RemoveLastPoints(1) != 0
}

func (w *Wire) RemoveLastPoints(count int) int {
	return w.shape.RemoveLastPoints(count)
}

func (w *Wire) Points() []vmath.Vec2i {
	points := w.shape.Points()
	// convert to int:
	pi := make([]vmath.Vec2i, len(points))
	for i, p := range points {
		pi[i] = p.Vec2i()
	}
	return pi
}

func (w *Wire) Point(idx int) vmath.Vec2i {
	return w.shape.Point(idx).Vec2i()
}

func (w *Wire) LastPoint() vmath.Vec2i {
	length := w.shape.Length()
	assert.True(length >= 2, "wire should not exist (too short: %d)", length)
	return w.shape.Point(length - 1).Vec2i()
}

// PointIndex returns the index of the given point on the line.
// Returns -1 if the point is not part of the line.
func (w *Wire) PointIndex(point vmath.Vec2i) int {
	fp := point.Vec2f()
	points := w.shape.Points()
	for idx, p := range points {
		if p == fp {
			return idx
		}
	}
	return -1
}

func (w *Wire) ClearPoints() {
	w.shape.ClearPoints()
}

// Length returns the number of points on the line
func (w *Wire) Length() int {
	return w.shape.Length()
}

func (w *Wire) Segments() []utils.LineSegment2i {
	points := w.shape.Points()
	if len(points) < 2 {
		return nil
	}

	segCount := len(points) - 1
	seg := make([]utils.LineSegment2i, segCount)

	for idx := 0; idx < segCount; idx++ {
		seg[idx].Start = points[idx].Vec2i()
		seg[idx].End = points[idx+1].Vec2i()
	}
	assert.True(seg[segCount-1].End.Vec2f() == points[len(points)-1], "Wrong segment calculation")
	return seg
}

// OnWire checks if the wire is placed on a specific point.
func (w *Wire) OnWire(point vmath.Vec2i) bool {
	fp := point.Vec2f()
	points := w.shape.Points()
	if len(points) == 1 && points[0] == fp {
		return true
	}
	for i := 1; i < len(points); i++ {
		a := points[i-1]
		b := points[i]
		dist := vmath.PointToLineSegmentDistance2D(a, b, fp)
		if dist < 0.1 { // All positions are based on integers, so this should work
			return true
		}
	}
	return false
}

// OnWireCorner checks if there is a wire corner/point on a specific point.
// wire corners include endpoints
func (w *Wire) OnWireCorner(point vmath.Vec2i) bool {
	fp := point.Vec2f()
	points := w.shape.Points()
	for _, p := range points {
		if p == fp {
			return true
		}
	}
	return false
}

// Overlaps checks if the given segment would overlap the wire.
// If segments only touch or cross, they do not overlap.
func (w *Wire) Overlaps(segment utils.LineSegment2i) bool {
	for idx := 1; idx < w.shape.Length(); idx++ {
		wireSeg := utils.LineSegment2i{
			Start: w.Point(idx - 1),
			End:   w.Point(idx),
		}
		if utils.SegmentOverlap(wireSeg, segment) {
			return true
		}
	}
	return false
}

func (a *Wire) Draw(renderState *nora.RenderState) {
	a.shape.Draw(renderState)
}

func (a *Wire) Destroy() {
	a.shape.Destroy()
}

func (w *Wire) Reset() {
	w.shape.ClearPoints()
	w.shape.SetColor(color.Black)
}
