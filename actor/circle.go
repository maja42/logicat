package actor

import (
	"github.com/maja42/nora"
	"github.com/maja42/nora/builtin/geometry/geo2d"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

type Circle struct {
	nora.Transform
	mesh nora.Mesh

	radius   float32
	segments int
	color    color.Color
}

func NewCircle(radius float32, segments int, color color.Color) *Circle {
	mat := nora.NewMaterial(shader.COL_2D)

	s := &Circle{
		mesh:     *nora.NewMesh(mat),
		radius:   radius,
		segments: segments,
		color:    color,
	}
	s.ClearTransform()
	s.updateGeometry()
	return s
}

func (a *Circle) Destroy() {
	a.mesh.Destroy()
}

func (a *Circle) SetColor(color color.Color) {
	a.color = color
	a.mesh.Material().Uniform4fColor("fragColor", color)
}

func (a *Circle) SetRadius(radius float32) {
	a.radius = radius
	a.updateGeometry()
}
func (a *Circle) SetSegments(segments int) {
	a.segments = segments
	a.updateGeometry()
}

func (a *Circle) updateGeometry() {
	a.mesh.SetGeometry(geo2d.Circle(vmath.Vec2f{}, a.radius, a.segments))
}

func (a *Circle) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.PushMulRight(a.GetTransform())
	a.mesh.Draw(renderState)
	renderState.TransformStack.Pop()
}
