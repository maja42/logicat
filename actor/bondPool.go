package actor

import (
	"github.com/maja42/logicat/theme"
	"github.com/maja42/logicat/utils/pool"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

const initialBondPoolSize = 20
const maxBondPoolSize = 100

// BondPool is a pool that supplies bond meshes.
// Unneeded objects should be returned to the pool.
// Retrieved objects are reset and therefore clean.
type BondPool struct {
	pool pool.Pool
}

func NewBondPool() *BondPool {
	newFunc := func() pool.Poolable {
		return NewCircle(theme.BondRadius, 32, color.Transparent)
	}
	resetFunc := func(bond pool.Poolable) {
		bond.(*Circle).SetColor(color.Transparent)
		bond.(*Circle).SetPosition(vmath.Vec3f{})
	}
	return &BondPool{
		pool: *pool.NewPool(initialBondPoolSize, maxBondPoolSize, newFunc, resetFunc),
	}
}

func (p *BondPool) Get() *Circle {
	return p.pool.Get().(*Circle)
}

func (p *BondPool) Put(bonds ...*Circle) {
	for _, b := range bonds {
		assert.True(b != nil, "cannot return nil to pool")
		p.pool.Put(b)
	}
}

func (p *BondPool) Destroy() {
	p.pool.Destroy()
}
