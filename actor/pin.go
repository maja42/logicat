package actor

import (
	"github.com/maja42/logicat/geom"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/logicat/theme"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/vmath"
)

type PinList map[*Pin]struct{}

func (p PinList) AddPins(pins []*Pin) {
	for _, pin := range pins {
		p[pin] = struct{}{}
	}
}

func (p PinList) Positions() []vmath.Vec2i {
	pos := make([]vmath.Vec2i, 0, len(p))
	for pin := range p {
		pos = append(pos, pin.SchemaPos())
	}
	return pos
}

type PinType int

const (
	InputPin = PinType(iota)
	OutputPin
)

type Pin struct {
	offsetPos vmath.Vec2i // local position relative to the component
	schemaPos vmath.Vec2i // position on the schematic

	pinMesh       nora.Mesh
	connectedMesh nora.Mesh

	pinType     PinType
	orientation geom.Orientation

	netFunc logic.NetFunc // TODO: define!

	netID uint64 // network this pin is connected to; 0 if not connected
}

// NewPin creates a new pin.
// offsetPos is used for rendering (model space) and therefore relative to the parent component.
func NewInputPin(offsetPos vmath.Vec2i, orientation geom.Orientation, marks geom.PinMark) *Pin {
	return newPin(offsetPos, orientation, marks, InputPin)
}

// NewPin creates a new pin.
// offsetPos is used for rendering (model space) and therefore relative to the parent component.
func NewOutputPin(offsetPos vmath.Vec2i, orientation geom.Orientation, marks geom.PinMark, netFunc logic.NetFunc) *Pin {
	pin := newPin(offsetPos, orientation, marks, OutputPin)
	pin.netFunc = netFunc
	return pin
}

// NewPin creates a new pin.
// offsetPos is used for rendering (model space) and therefore relative to the parent component.
func newPin(offsetPos vmath.Vec2i, orientation geom.Orientation, marks geom.PinMark, pinType PinType) *Pin {
	mat := nora.NewMaterial(shader.COL_2D)
	s := &Pin{
		offsetPos:     offsetPos,
		schemaPos:     offsetPos, // assume component has position [0,0]
		pinMesh:       *nora.NewMesh(mat),
		connectedMesh: *nora.NewMesh(mat),
		pinType:       pinType,
		orientation:   orientation,
	}
	color := theme.PinColorInput
	if pinType == OutputPin {
		color = theme.PinColorOutput
	}
	mat.Uniform4fColor("fragColor", color)
	s.pinMesh.SetGeometry(geom.Pin(offsetPos, orientation, marks, false))
	s.connectedMesh.SetGeometry(geom.Pin(offsetPos, orientation, marks, true))
	return s
}

func (m *Pin) Destroy() {
	m.pinMesh.Destroy()
}

// SetSchemaPos informs the pin about it's position on the schematic.
// Does not affect the geometry.
func (m *Pin) SetSchemaPos(pos vmath.Vec2i) {
	m.schemaPos = pos
}

// SetParentPos informs the pin about it's position on the schematic, by providing the parent's position.
// Returns the resulting schema position.
// Does not affect the geometry.
func (m *Pin) SetParentPos(pos vmath.Vec2i) vmath.Vec2i {
	m.schemaPos = pos.Add(m.offsetPos)
	return m.schemaPos
}

// SchemaPos returns the component's (global) position on the schematic.
func (m *Pin) SchemaPos() vmath.Vec2i {
	return m.schemaPos
}

// OffsetPos returns the pin's position relative to it's parent component's origin.
func (m *Pin) OffsetPos() vmath.Vec2i {
	return m.offsetPos
}

func (m *Pin) Orientation() geom.Orientation {
	return m.orientation
}

func (m *Pin) Type() PinType {
	return m.pinType
}

func (m *Pin) NetFunc() logic.NetFunc {
	assert.True(m.pinType == OutputPin, "cannot retrieve netFunc from this pin")
	assert.True(m.netFunc != nil, "pin should contain valid netFunc, but does not")
	return m.netFunc
}

func (m *Pin) Draw(renderState *nora.RenderState) {
	if m.netID == 0 {
		m.pinMesh.Draw(renderState)
	} else {
		m.connectedMesh.Draw(renderState)
	}
}

// ConnectToNetwork informs the pin that it's connected to the given network.
func (m *Pin) ConnectToNetwork(netID uint64) {
	assert.True(netID > 0, "invalid netID")
	m.netID = netID
}

// Disconnect informs the pin that it's not connected to any network anymore.
func (m *Pin) Disconnect() {
	assert.True(m.netID > 0, "not connected")
	m.netID = 0
}

// ConnectedNetwork returns the network ID this pin is connected to.
// Returns zero if the pin is not connected.
func (m *Pin) ConnectedNetwork() uint64 {
	return m.netID
}

// IsConnectedToNetwork returns true if the pin is connected to a network.
func (m *Pin) IsConnectedToNetwork() bool {
	return m.netID != 0
}
