package actor

import (
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

// NewNetworkMeshes handles the graphical components of a network.
// It is responsible for creating wires and bonds and placing them where they should be.
// Tries to reduce objects and draw calls.
type NetworkMeshes struct {
	wirePool *WirePool
	bondPool *BondPool
	color    color.Color

	wires []*Wire
	bonds map[vmath.Vec2i]*Circle
}

func NewNetworkMeshes(wirePool *WirePool, bondPool *BondPool, color color.Color) *NetworkMeshes {
	return &NetworkMeshes{
		wirePool: wirePool,
		bondPool: bondPool,
		color:    color,
		wires:    make([]*Wire, 0),
		bonds:    make(map[vmath.Vec2i]*Circle),
	}
}

func (m *NetworkMeshes) Merge(other *NetworkMeshes) {
	m.wires = append(m.wires, other.wires...)
	for _, w := range other.wires {
		w.SetColor(m.color)
	}
	other.wires = other.wires[:0]

	for pos, bond := range other.bonds {
		_, exists := m.bonds[pos]
		assert.False(exists, "found two bonds on same position during merging")
		bond.SetColor(m.color)
		m.bonds[pos] = bond
	}
	other.bonds = make(map[vmath.Vec2i]*Circle)
}

func (m *NetworkMeshes) Rebuild(segments []utils.LineSegment2i) {
	m.RebuildWires(segments)
	m.RebuildBonds(segments)
}

func (m *NetworkMeshes) RebuildWires(segments []utils.LineSegment2i) {
	for _, wire := range m.wires {
		m.wirePool.Put(wire)
	}
	m.wires = nil
	for _, seg := range segments {
		m.AddSegment(segments, seg)
	}
}

func (m *NetworkMeshes) AddSegment(segments []utils.LineSegment2i, segment utils.LineSegment2i) {
	defer func() {
		m.addBondIfNeeded(segments, segment.Start)
		m.addBondIfNeeded(segments, segment.End)
	}()

	startWire, startFront := findWireEndpoint(m.wires, segment.Start)
	endWire, endFront := findWireEndpoint(m.wires, segment.End)

	if startWire == nil && endWire == nil { // no connection
		m.addNewWire(segment)
		return
	}

	if startWire == endWire { // loop that connects the two endings
		endWire = nil // dont merge a wire with itself
	}

	if startWire != nil && endWire != nil { // merge two wires; ignore the segment
		points := endWire.Points()
		m.removeWire(endWire)
		if startFront == endFront {
			utils.ReverseVec2iSlice(points)
		}
		if startFront {
			startWire.AddPointsAtFront(points...)
		} else {
			startWire.AddPoints(points...)
		}
		return
	}

	// attach segment to existing wire(s):
	if startWire != nil {
		if startFront {
			startWire.AddPointsAtFront(segment.End)
		} else {
			startWire.AddPoints(segment.End)
		}
	} else if endWire != nil {
		if endFront {
			endWire.AddPointsAtFront(segment.Start)
		} else {
			endWire.AddPoints(segment.Start)
		}
	}
}

// AddBond adds a new bond at a given location.
// It is not checked/validated if the bond actually needs to be there.
func (m *NetworkMeshes) AddBond(pos vmath.Vec2i) {
	m.addBondIfMissing(pos)
}

func findWireEndpoint(wires []*Wire, pos vmath.Vec2i) (*Wire, bool) {
	for _, wire := range wires {
		if wire.Point(0) == pos {
			return wire, true
		}
		if wire.LastPoint() == pos {
			return wire, false
		}
	}
	return nil, false
}

func (m *NetworkMeshes) addNewWire(segment utils.LineSegment2i) {
	wire := m.wirePool.Get()
	wire.AddPoints(segment.Start, segment.End)
	wire.SetColor(m.color)
	m.wires = append(m.wires, wire)
}

func (m *NetworkMeshes) removeWire(wire *Wire) {
	for idx, w := range m.wires {
		if w == wire {
			m.wires = append(m.wires[:idx], m.wires[idx+1:]...)
			m.wirePool.Put(w)
			return
		}
	}
	assert.Fail("did not find wire for removal")
}

func (m *NetworkMeshes) RebuildBonds(segments []utils.LineSegment2i) {
	for _, bond := range m.bonds {
		m.bondPool.Put(bond)
	}
	m.bonds = make(map[vmath.Vec2i]*Circle)

	// Bonds can only be at the end of a segment
	for _, seg := range segments {
		m.addBondIfNeeded(segments, seg.Start)
		m.addBondIfNeeded(segments, seg.End)
	}
}

func (m *NetworkMeshes) addBondIfNeeded(segments []utils.LineSegment2i, pos vmath.Vec2i) {
	// A bond is needed if:
	//  - A segment ends or starts on this position (meaning that the segments are connected and not only crossing)
	//	- The position has 3 or more wires on it.
	// Note:
	//  - It is guaranteed (by network.go) that segments don't "pass trough" a position where another wire ends (and a bond might be needed)
	//		Therefore, we only need to consider the endpoints of segments, not the positions in-between.
	//		Multiple segments can be combined into a single "wire-segment" though, meaning that it's not sufficient to only check wire points.
	//	- A single wire can pass the same position multiple times (wire crosses itself), creating a loop.

	count := 0
	for _, seg := range segments {
		if seg.Start == pos {
			count++
		}
		if seg.End == pos {
			count++
		}
		if count >= 3 {
			break
		}
	}
	if count >= 3 {
		m.addBondIfMissing(pos)
	}
}

func (m *NetworkMeshes) addBondIfMissing(pos vmath.Vec2i) {
	if _, ok := m.bonds[pos]; ok {
		return // exists
	}
	bond := m.bondPool.Get()
	bond.SetPosition(pos.Vec2f().Vec3f(0))
	bond.SetColor(m.color)
	m.bonds[pos] = bond
}

func (m *NetworkMeshes) Destroy() {
	m.wirePool.Put(m.wires...)
	m.wires = nil
	m.wirePool = nil
	for _, bond := range m.bonds {
		m.bondPool.Put(bond)
	}
	m.bonds = nil
	m.bondPool = nil
}

func (m *NetworkMeshes) Draw(renderState *nora.RenderState) {
	for _, w := range m.wires {
		w.Draw(renderState)
	}
	for _, b := range m.bonds {
		b.Draw(renderState)
	}
}
