package actor

import (
	"fmt"

	"github.com/maja42/logicat/logic"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
	"github.com/sirupsen/logrus"
	"go.uber.org/atomic"
)

var netNameCounter atomic.Uint64

//type NetworkList map[*Network]struct{}

//type NetworkStatus int
//
//const (
//	NetworkDisconnected = NetworkStatus(iota)
//	NetworkInactive
//	NetworkActive
//	NetworkConflict
//	//WirePlacement
//)

type Domain interface {
	DefineNetwork(netID uint64, netFunc logic.NetFunc)
	RemoveNetwork(netID uint64)
}

type Network struct {
	netID uint64 // Unique identifier of this network (>=1)
	name  string

	domain Domain

	outputPin *Pin   // The output pin which sets this network's value
	inputPins []*Pin // Input pins connected to this network

	segments []utils.LineSegment2i

	meshes NetworkMeshes // graphical component
}

func NewNetwork(domain Domain, wirePool *WirePool, bondPool *BondPool) *Network {
	w := &Network{
		netID: netNameCounter.Inc(),

		domain: domain,

		meshes: *NewNetworkMeshes(wirePool, bondPool, utils.RandomColor()),
	}
	domain.DefineNetwork(w.netID, nil)
	return w
}

// NetID returns the unique ID of this network.
func (a *Network) NetID() uint64 {
	return a.netID
}

// RealName returns the user-defined name or empty string if there is none.
func (a *Network) RealName() string {
	return a.name
}

// Name returns the name of this network; Returns an auto-generated name if the user did not define one.
// The auto-generated name always reflects the netID, even if networks are merged together.
func (a *Network) Name() string {
	if a.name != "" {
		return a.name
	}
	return fmt.Sprintf("Net %d", a.netID)
}

// SetName changes the network's name to a user-defined name
func (a *Network) SetName(name string) {
	a.name = name
}

func (a *Network) OutputPin() *Pin {
	assert.True(a.outputPin == nil || a.outputPin.ConnectedNetwork() == a.netID, "output pin is inconsistent with netID")
	return a.outputPin
}

func (a *Network) BitWidth() int {
	return 64 // TODO!
}

func (a *Network) SetOutputPin(pin *Pin) {
	if pin == nil { // disconnect
		a.domain.DefineNetwork(a.netID, nil)
		a.outputPin.Disconnect()
		a.outputPin = nil
		return
	}

	if a.outputPin == pin { // already connected to this network
		assert.True(pin.ConnectedNetwork() == a.netID, "conflicting: output pin network does not reference this network")
		return
	}
	assert.True(pin.pinType == OutputPin, "provided pin is not an output pin")
	assert.False(pin.IsConnectedToNetwork(), "output pin is already connected to a network")
	assert.True(a.Contains(pin.SchemaPos()), "output pin does not touch the network")

	a.domain.DefineNetwork(a.netID, pin.NetFunc()) // update netFunc that drives this network

	pin.ConnectToNetwork(a.netID)
	a.outputPin = pin
	a.splitSegments(pin.schemaPos)
}

func (a *Network) AddInputPin(pin *Pin) {
	if pin.ConnectedNetwork() == a.netID { // already connected to this network
		return
	}
	assert.True(pin.pinType == InputPin, "provided pin is not an input pin")
	assert.False(pin.IsConnectedToNetwork(), "input pin is already connected to a network")

	assert.True(a.Contains(pin.SchemaPos()), "input pin does not touch the network")

	pin.ConnectToNetwork(a.netID)
	a.inputPins = append(a.inputPins, pin)
	a.splitSegments(pin.schemaPos)
}

func (a *Network) InputPins() []*Pin {
	return a.inputPins
}

// AddWireSegment adds a new segment to the network.
// Assumes that all connected/touched wires are already merged into this network.
func (a *Network) AddWireSegment(segment utils.LineSegment2i) {
	// Split the segment into multiple smaller segments at crossings that are added individually.
	// This ensures that we can calculate bonds and wires without crosses in the middle of segments.

	points := utils.PointsOnSegment(segment)
	start := points[0]
	for idx := 1; idx < len(points)-1; idx++ {
		p := points[idx]
		if !a.containsSegmentEndpoint(p) {
			continue
		}
		assert.False(a.containsPin(p), "pins should always cause a segment to end, but doesn't at %v", p.String())

		a.addNonCrossingWireSegment(utils.LineSegment2i{
			Start: start,
			End:   p,
		})
		start = p
	}
	a.addNonCrossingWireSegment(utils.LineSegment2i{
		Start: start,
		End:   points[len(points)-1],
	})
}

// containsSegmentEndpoint checks if the given position is the endpoint of a segment.
// If so, this position automatically connects to any other segment using this position.
func (a *Network) containsSegmentEndpoint(pos vmath.Vec2i) bool {
	for _, seg := range a.segments {
		if seg.Start == pos || seg.End == pos {
			return true
		}
	}
	return false
}

// SegmentEndpoints returns all segment endpoints.
func (a *Network) SegmentEndpoints() map[vmath.Vec2i]struct{} {
	endpoints := make(map[vmath.Vec2i]struct{}, len(a.segments)*2)
	for _, seg := range a.segments {
		endpoints[seg.Start] = struct{}{}
		endpoints[seg.End] = struct{}{}
	}
	return endpoints
}

// addNonCrossingWireSegment adds a new segment.
// It is ensured that this segment does not connect to any other segment in it's middle (there are no segment endpoints along its line).
func (a *Network) addNonCrossingWireSegment(segment utils.LineSegment2i) {
	assert.True(segment.Length() > 0, "cannot add segment with zero length")
	if a.Overlaps(segment) { // Already exists
		return
	}

	// Every segment endpoint automatically connects to any other wire on that position.
	// Wires can only connect on their endpoints. Therefore, we need to split all other wires that currently cross the new segment's endpoint.
	startCount, startSegIdx := a.splitSegments(segment.Start)
	endCount, endSegIdx := a.splitSegments(segment.End)

	segIdx := len(a.segments)
	a.segments = append(a.segments, segment)

	if startCount == 1 {
		if a.tryMergeSegments(startSegIdx, segIdx) {
			segIdx = startSegIdx
		}
	}
	if endCount == 1 {
		a.tryMergeSegments(endSegIdx, segIdx)
	}

	a.meshes.AddSegment(a.segments, segment)
}

// Overlaps checks if the given segment would overlap any existing wire.
// If segments only touch or cross, they do not overlap.
func (a *Network) Overlaps(segment utils.LineSegment2i) bool {
	for _, seg := range a.segments {
		if utils.SegmentOverlap(segment, seg) {
			return true
		}
	}
	return false
}

func (a *Network) tryMergeSegments(segIdx1, segIdx2 int) bool {
	seg1 := a.segments[segIdx1]
	seg2 := a.segments[segIdx2]

	if !seg1.IsLineSegmentCollinear(seg2) {
		return false
	}

	merged := false
	if seg1.Start == seg2.Start {
		if a.containsPin(seg1.Start) {
			return false
		}
		a.segments[segIdx1].Start = seg2.End
		merged = true
	} else if seg1.Start == seg2.End {
		if a.containsPin(seg1.Start) {
			return false
		}
		a.segments[segIdx1].Start = seg2.Start
		merged = true
	} else if seg1.End == seg2.Start {
		if a.containsPin(seg1.End) {
			return false
		}
		a.segments[segIdx1].End = seg2.End
		merged = true
	} else if seg1.End == seg2.End {
		if a.containsPin(seg1.End) {
			return false
		}
		a.segments[segIdx1].End = seg2.Start
		merged = true
	}

	if merged { // remove second segment
		a.segments = append(a.segments[:segIdx2], a.segments[segIdx2+1:]...)
	}
	return merged
}

// splitSegments splits all segments that pass though the given location into two smaller segments.
// Returns the number of segments connecting/touching at the given location after splitting.
// Also returns the index of one of the segments touching the location or -1 if there are none.
func (a *Network) splitSegments(pos vmath.Vec2i) (int, int) {
	// Segments need to be split if they get connected to a new segment in their middle.
	// Segments can only be connected on their endpoints.

	// When splitting a segment it's not required to inform the "NetworkMeshes", as it has no influence on the graphics.

	count := 0
	touchIdx := -1
	for idx, seg := range a.segments {
		if !seg.Contains(pos) {
			continue
		}
		if seg.Start == pos || seg.End == pos {
			count++
			touchIdx = idx
			continue
		}
		count += 2
		// Split segment!
		a.segments = append(a.segments, utils.LineSegment2i{
			Start: pos,
			End:   seg.End,
		})
		a.segments[idx].End = pos
		touchIdx = idx
	}
	assert.True(count >= 0 && count <= 8, "corrupt segment data") // there can never be more than 8 segments on a single location
	return count, touchIdx
}

// Contains checks if the network uses/touches the given position.
func (a *Network) Contains(pos vmath.Vec2i) bool {
	for _, seg := range a.segments {
		if seg.Contains(pos) {
			return true
		}
	}
	return false
}

// containsPin checks if the network connects to a pin at the given position.
func (a *Network) containsPin(pos vmath.Vec2i) bool {
	if a.outputPin != nil && a.outputPin.SchemaPos() == pos {
		return true
	}
	for _, inputPin := range a.inputPins {
		if inputPin.SchemaPos() == pos {
			return true
		}
	}
	return false
}

func (a *Network) SegmentCount() int {
	return len(a.segments)
}

func (a *Network) Segments() []utils.LineSegment2i {
	return a.segments
}

//// Segments returns the first n segments at the given position.
//// If max is 8 or higher, all segments are returned as there can't be more than 8 segments at a given location.
//func (a *Network) Segments(pos vmath.Vec2i, max int) []utils.LineSegment2i {
//	var segments []utils.LineSegment2i
//
//	for _, seg := range a.segments {
//		if seg.Contains(pos) {
//			segments = append(segments, seg)
//			if len(segments) >= max {
//				return segments
//			}
//		}
//	}
//	return segments
//}

// StolenSegment contains all information about a stolen (removed) segment and its neighbours.
type StolenSegment struct {
	Network *Network
	Segment utils.LineSegment2i
	// If the neighbours have zero length, they don't exist.
	StartNeighbour utils.LineSegment2i
	EndNeighbour   utils.LineSegment2i
}

// StealSegment removes the segment at the given position, as well as its two neighbours if there is only one on each side.
// Stealing the segments does not split the network into two, it only removes connected pins.
// It's expected that the segments (or equivalent ones) are added back later such that all network parts are connected again.
// Intended for segment displacement (movement).
func (a *Network) StealSegment(pos vmath.Vec2i) *StolenSegment {
	var segment utils.LineSegment2i
	var found bool

	for idx, seg := range a.segments {
		if seg.Contains(pos) {
			segment = seg
			found = true
			a.removeSegment(idx) // remove
			break
		}
	}
	if !found {
		return nil
	}

	info := &StolenSegment{
		Network: a,
		Segment: segment,
	}

	var startIdx, endIdx int
	var foundStart, foundEnd int

	for idx, seg := range a.segments {
		if seg.Start == segment.Start || seg.End == segment.Start { // found start neighbour
			info.StartNeighbour = seg
			startIdx = idx
			foundStart++
		} else if seg.Start == segment.End || seg.End == segment.End { // found end neighbour
			info.EndNeighbour = seg
			endIdx = idx
			foundEnd++
		}
		if foundStart >= 2 && foundEnd >= 2 {
			break
		}
	}

	if foundStart == 1 && !a.containsPin(segment.Start) { // Only one start neighbour
		a.removeSegment(startIdx)
		if endIdx > startIdx {
			endIdx--
		}
	} else {
		info.StartNeighbour = utils.LineSegment2i{}
	}
	if foundEnd == 1 && !a.containsPin(segment.End) { // Only one end neighbour
		a.removeSegment(endIdx)
	} else {
		info.EndNeighbour = utils.LineSegment2i{}
	}
	a.removeDisconnectedPins()
	a.meshes.Rebuild(a.segments)
	return info
}

// removeSegment removes a segment from the network.
// Does not remove any disconnected pins.
// Does not split the network into two, even if it would be disconnected.
func (a *Network) removeSegment(idx int) {
	a.segments = append(a.segments[:idx], a.segments[idx+1:]...)
}

func (a *Network) removeDisconnectedPins() {
	idx := 0
	for _, inputPin := range a.inputPins {
		if a.Contains(inputPin.SchemaPos()) {
			a.inputPins[idx] = inputPin
			idx++
		} else {
			inputPin.Disconnect()
		}
	}
	for j := idx; j < len(a.inputPins); j++ {
		a.inputPins[j] = nil
	}
	a.inputPins = a.inputPins[:idx]

	if a.outputPin != nil {
		if !a.Contains(a.outputPin.SchemaPos()) {
			a.SetOutputPin(nil)
		}
	}
}

// Merge merges another network into this one. They are now connected.
// Note: The new wire segments or bonds that are responsible for the merge must be inserted afterwards.
func (a *Network) Merge(other *Network) {
	assert.True(a != other, "Cannot merge network with itself")

	logrus.Infof("Merging network %q into %q", other.Name(), a.Name())

	a.segments = append(a.segments, other.segments...)
	other.segments = nil

	a.meshes.Merge(&other.meshes)

	if a.outputPin == nil && other.outputPin != nil {
		assert.True(other.outputPin.ConnectedNetwork() == other.netID, "invalid state of netID between network and output pin")
		outputPin := other.outputPin
		other.SetOutputPin(nil)
		a.SetOutputPin(outputPin)
	} else {
		assert.True(a.outputPin == nil || other.outputPin == nil || other.outputPin == a.outputPin, "incompatible networks")
	}

	for _, inputPin := range other.inputPins {
		assert.True(inputPin.ConnectedNetwork() == other.netID, "invalid state of netID between network and input pin")
		inputPin.Disconnect()
		a.AddInputPin(inputPin)
	}
	other.inputPins = nil
}

// CanBond returns true if the given location contains at least two (crossing) segments that can be bonded.
// Returns false if there is already a bond, or if there are less than two segments.
func (a *Network) CanBond(pos vmath.Vec2i) bool {
	found := false
	for _, seg := range a.segments {
		if seg.Contains(pos) {
			if seg.Start == pos || seg.End == pos {
				// Either there is already a bond, or it's not possible to place one there
				return false
			}
			if found { // this is the second segment
				return true
			}
			found = true
		}
	}
	return false
}

// Bond creates a new bond at the given location where two or more segments cross.
func (a *Network) Bond(pos vmath.Vec2i) {
	assert.True(a.CanBond(pos), "network cannot be bonded here")

	count, _ := a.splitSegments(pos)
	assert.True(count > 2 && count%2 == 0, "bonds can be inserted at positions with crossing wires only")
	a.meshes.AddBond(pos)
}

func (a *Network) Destroy() {
	a.SetName("<DESTROYED " + a.Name() + ">")
	a.domain.RemoveNetwork(a.netID)
	a.meshes.Destroy()
}

func (a *Network) Draw(renderState *nora.RenderState) {
	a.meshes.Draw(renderState)
}
