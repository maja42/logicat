package logic

import "github.com/maja42/nora/assert"

// Info:
//	- a network can have zero or one output pin (=driver pin)
//  - output-pins can be connected directly with input-pins without a wire (a network still exists between them)
//	- output-pins that are not connected to anything don't need to be evaluated
//	- networks that don't have an output-pin always have the zero-value

// Domain holds the values of every network and performs physic ticks for simulation
type Domain struct {
	netValues map[uint64]NetVal

	netValueDrivers map[uint64]NetFunc
	nextNetValues   map[uint64]NetVal
}

// NewDomain creates a new domain
func NewDomain() *Domain {
	domain := &Domain{}
	domain.Clear()
	return domain
}

func (d *Domain) NetworkCount() int {
	return len(d.netValues)
}

// DefineNetwork adds or reconfigures a network.
func (d *Domain) DefineNetwork(netID uint64, netFunc NetFunc) {
	assert.True(netID != 0, "netID 0 is not allowed (0 = no network)")

	d.netValues[netID] = 0
	d.nextNetValues[netID] = 0
	if netFunc == nil {
		delete(d.netValueDrivers, netID)
	} else {
		d.netValueDrivers[netID] = netFunc
	}
}

// RemoveNetwork removes a network.
func (d *Domain) RemoveNetwork(netID uint64) {
	_, ok := d.netValues[netID]
	assert.True(ok, "can't remove network %d, which never existed", netID)

	delete(d.netValues, netID)
	delete(d.nextNetValues, netID)
	delete(d.netValueDrivers, netID)
}

// Clear removes all networks.
func (d *Domain) Clear() {
	d.netValues = make(map[uint64]NetVal)
	d.netValueDrivers = make(map[uint64]NetFunc)
	d.nextNetValues = make(map[uint64]NetVal)
}

// NetValue returns the current value of a specific network
func (d *Domain) NetValue(netID uint64) NetVal {
	return d.netValues[netID]
}

// Tick performs a physics tick that updates the value of every network
func (d *Domain) Tick() {
	assert.True(len(d.netValues) == len(d.nextNetValues) && len(d.netValues) >= len(d.netValueDrivers), "corrupt domain state")

	for net, driver := range d.netValueDrivers { // can be parallelized
		d.nextNetValues[net] = driver(d.NetValue)
	}
	// swap state:
	d.netValues, d.nextNetValues = d.nextNetValues, d.netValues
}

// NetValueQueryFunc returns the value of the given network.
type NetValueQueryFunc func(netID uint64) NetVal

// NetFunc calculates the value of a specific network for the next tick.
// The given queryFunc can be used to query current network values.
type NetFunc func(netQuery NetValueQueryFunc) NetVal
