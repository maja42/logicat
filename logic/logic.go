package logic

import (
	"fmt"
	"math"
	"strconv"

	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath/math32"
)

// The max. number of lines/bits in a single network.
const MaxNetSize = 64

const MaxNetVal NetVal = math.MaxUint64

// NetVal represents the value of a single network.
// Networks can consist of multiple lines/bits that need to be represented.
type NetVal uint64

func (n NetVal) Bit(bit int) bool {
	return n&(1<<bit) != 0
}

func (n *NetVal) SetBit(bit int) NetVal {
	*n = *n | (1 << bit)
	return *n
}

func (n *NetVal) ClearBit(bit int) NetVal {
	*n = *n &^ (1 << bit)
	return *n
}

func (n *NetVal) ToggleBit(bit int) NetVal {
	*n = *n ^ (1 << bit)
	return *n
}

type DisplayFormat int

const (
	HEXDisplay = DisplayFormat(iota)
	BINDisplay
	DECDisplay
)

func (n NetVal) String(bitWidth int, format DisplayFormat) string {
	assert.True(bitWidth >= 1 && bitWidth <= MaxNetSize, "invalid bit width")

	if bitWidth == 1 {
		return strconv.Itoa(int(n)) // No prefix
	}

	digits := requiredDigits(bitWidth, format)
	var fmtString string
	switch format {
	case HEXDisplay:
		fmtString = "0x%0" + strconv.Itoa(digits) + "X"
	case BINDisplay:
		fmtString = "0b%0" + strconv.Itoa(digits) + "b"
	case DECDisplay:
		fmtString = "%0" + strconv.Itoa(digits) + "d"
	}
	return fmt.Sprintf(fmtString, n)
}

// requiredDigits returns the number of digits (without any prefix like '0x') to display the value in the given format
func requiredDigits(bitWidth int, displayFmt DisplayFormat) int {
	if bitWidth == 1 { // fast track
		return 1
	}

	var digits float32
	switch displayFmt {
	case HEXDisplay:
		digits = float32(bitWidth) / 4
	case BINDisplay:
		return bitWidth
	case DECDisplay:
		max := (0x01 << bitWidth) - 1
		digits = math32.Log10(float32(max))
	}
	return int(math32.Ceil(digits))
}
