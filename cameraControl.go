package main

import (
	"github.com/maja42/glfw"
	"github.com/maja42/nora"
	"github.com/maja42/vmath"
)

const (
	zoomSensitivity   float32 = 0.2
	scrollSensitivity float32 = 0.1
)

type CameraMovement struct {
	cam            *nora.OrthoCamera
	interactionSys *nora.InteractionSystem

	scrollCBID    nora.CallbackID
	mousemoveCBID nora.CallbackID
}

func NewCameraMovement(cam *nora.OrthoCamera, interactionSys *nora.InteractionSystem) *CameraMovement {
	m := &CameraMovement{
		cam:            cam,
		interactionSys: interactionSys,
	}

	m.scrollCBID = m.interactionSys.OnScroll(m.onMouseScroll)
	m.mousemoveCBID = m.interactionSys.OnMouseMoveEvent(m.onMouseMove)
	return m
}

func (m *CameraMovement) Destroy() {
	m.interactionSys.RemoveScrollEventFunc(m.scrollCBID)
	m.interactionSys.RemoveMouseMoveEventFunc(m.mousemoveCBID)
}

func (m *CameraMovement) onMouseScroll(offset vmath.Vec2i) {
	if offset[1] == 0 {
		return
	}

	if m.interactionSys.IsKeyPressed(glfw.KeyLeftControl) {
		mouseClip := m.interactionSys.MousePosClipSpace()
		mouseWorld := m.cam.ClipSpaceToWorldSpace(mouseClip)
		m.Zoom(mouseWorld, float32(offset[1])*zoomSensitivity)
	} else if m.interactionSys.IsKeyPressed(glfw.KeyLeftShift) { // horizontally
		m.Scroll(vmath.Vec2f{float32(offset[1]) * scrollSensitivity, 0})
	} else { // vertically
		m.Scroll(vmath.Vec2f{0, float32(-offset[1]) * scrollSensitivity})
	}

}

func (m *CameraMovement) onMouseMove(pos, movement vmath.Vec2i) {
	it := m.interactionSys
	if !it.IsMouseButtonPressed(glfw.MouseButtonRight) {
		return
	}
	clipDist := it.WindowSpaceDistToClipSpaceDist(movement.Vec2f())
	worldDist := m.cam.ClipSpaceDistToWorldSpaceDist(clipDist)
	m.cam.Move(worldDist)
}

// Changes the camera zoom. Ensures that the origin (worldspace) does not move relative to the camera.
// If amount > 0, the zoom level increases, making everything bigger.
func (m *CameraMovement) Zoom(origin vmath.Vec2f, amount float32) {
	zoom := 1 - amount // percent
	oldOrthoWidth := m.cam.OrthoWidth()
	newOrthoWidth := oldOrthoWidth * zoom

	camPos := m.cam.Position()
	camToOldOrigin := origin.Sub(camPos)
	camToNewOrigin := camToOldOrigin.MulScalar(zoom)

	movement := camToOldOrigin.Sub(camToNewOrigin)

	m.cam.Move(movement.MulScalar(-1))
	m.cam.SetOrthoWidth(newOrthoWidth)
}

func (m *CameraMovement) Scroll(val vmath.Vec2f) {
	ortho := m.cam.OrthoWidth()
	movement := val.MulScalar(ortho) // make movement relative to zoom level
	m.cam.Move(movement)
}
