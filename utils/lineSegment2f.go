package utils

import "C"
import (
	"fmt"

	"github.com/maja42/vmath"
)

// LineSegment2f represents a line segment.
type LineSegment2f struct {
	Start vmath.Vec2f
	End   vmath.Vec2f
}

func (l LineSegment2f) String() string {
	return fmt.Sprintf("Seg2i[%s -> %s]", l.Start.String(), l.End.String())
}

// Format the line segment to a string.
func (l LineSegment2f) Format(segmentFormat, vecFormat string) string {
	return fmt.Sprintf(segmentFormat, l.Start.Format(vecFormat), l.End.Format(vecFormat))
}

// LineSegment2i returns an integer representation of the line segment.
// Decimals are truncated.
func (l LineSegment2f) LineSegment2i() LineSegment2i {
	return LineSegment2i{
		Start: l.Start.Vec2i(),
		End:   l.End.Vec2i(),
	}
}

// Round returns an integer representation of the line segment.
// Decimals are rounded.
func (l LineSegment2f) Round() LineSegment2i {
	return LineSegment2i{
		Start: l.Start.Round(),
		End:   l.End.Round(),
	}
}

// Line converts the line segment into a line.
func (l LineSegment2f) Line() Line2f {
	return Line2f{
		Point: l.Start,
		Vec:   l.Vec(),
	}
}

// Vec returns a vector from the start-point to the end-point of the segment.
func (l LineSegment2f) Vec() vmath.Vec2f {
	return l.End.Sub(l.Start)
}

// Length returns the segment's length.
func (l LineSegment2f) Length() float32 {
	return l.Vec().Length()
}

// SquareLength returns the segment's squared length.
func (l LineSegment2f) SquareLength() float32 {
	return l.Vec().SquareLength()
}

// HasZeroLength returns true if the segment has a length of zero.
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) HasZeroLength() bool {
	return l.Start.Equal(l.End)
}

// IsHorizontal returns true if the line is horizontal (same y coordinate).
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) IsHorizontal() bool {
	return vmath.Equalf(l.Start[1], l.End[1])
}

// IsVertical returns true if the line is horizontal (same x coordinate).
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) IsVertical() bool {
	return vmath.Equalf(l.Start[0], l.End[0])
}

// Project projects the line segment onto a given vector/axis.
func (l LineSegment2f) Project(v vmath.Vec2f) LineSegment2f {
	return LineSegment2f{
		Start: l.Start.Project(v),
		End:   l.End.Project(v),
	}
}

// IsPointCollinear checks if the given point is on the same line as this segment.
// "Line" is defined as expanding indefinitely on both sides of the segment.
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) IsPointCollinear(pos vmath.Vec2f) bool {
	lvec := l.Vec()
	pvec := pos.Sub(l.Start)
	return vmath.Equalf(lvec[0]*pvec[1], lvec[1]*pvec[0]) // lvec.MagCross(pvec) == 0

}

// IsLineSegmentCollinear checks if the two segments are on the same line.
// "Line" is defined as expanding indefinitely on both sides of the segment.
// Lines that are collinear are also parallel.
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) IsLineSegmentCollinear(other LineSegment2f) bool {
	return l.IsPointCollinear(other.Start) && l.IsPointCollinear(other.End)
}

// Contains checks if the given position is on the line segment.
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) Contains(pos vmath.Vec2f) bool {
	lvec := l.Vec()
	pvec := pos.Sub(l.Start)
	if !vmath.Equalf(lvec[0]*pvec[1], lvec[1]*pvec[0]) { // lvec.MagCross(pvec) != 0
		// Not collinear
		return false
	}
	if lvec.Dot(pvec) < 0 { // point is behind line segment
		return false
	}
	if lvec.SquareLength() < pvec.SquareLength() {
		return false
	}
	return true
}

// Intersect returns the intersection point between two line segments or false if they don't intersect.
// Returns the point with the lowest x-position if the segments overlap.
// Uses the default Epsilon as relative tolerance.
func (l LineSegment2f) Intersect(other LineSegment2f) (vmath.Vec2f, bool) {
	// Source: https://gamedev.stackexchange.com/a/117294/39091

	a := l.Start
	b := l.End
	c := other.Start
	d := other.End

	r1a := (a[1] - c[1]) * (d[0] - c[0])
	r1b := (a[0] - c[0]) * (d[1] - c[1])
	r1 := r1a - r1b

	r2a := (b[0] - a[0]) * (d[1] - c[1])
	r2b := (b[1] - a[1]) * (d[0] - c[0])
	r2 := r2a - r2b

	if vmath.Equalf(r2a, r2b) { // parallel
		if vmath.Equalf(r1a, r1b) { // collinear
			// Check overlap. Project both segments onto x-axis:
			a := l.Project(vmath.Vec2f{1, 0})
			b := other.Project(vmath.Vec2f{1, 0})
			if b.Start[0] <= a.End[0] || b.End[1] >= a.Start[0] { // overlap. Return the intersection point with the lowest X
				if a.Start[0] < b.Start[0] {
					return b.Start, true
				} else {
					return a.Start, true
				}
			}
			return vmath.Vec2f{}, false
		}
		return vmath.Vec2f{}, false
	}

	r := r1 / r2
	if r < 0 || r > 1 { // no intersection on segment "l"
		return vmath.Vec2f{}, false
	}

	s1 := (a[1]-c[1])*(b[0]-a[0]) - (a[0]-c[0])*(b[1]-a[1])
	s2 := r2
	s := s1 / s2
	if s < 0 || s > 1 { // no intersection on segment "other"
		return vmath.Vec2f{}, false
	}

	return vmath.Vec2f{
		a[0] + r*(b[0]-a[0]),
		a[1] + r*(b[1]-a[1]),
	}, true
}

// IntersectLine returns the intersection point between the line segment and the given line.
// Returns false if they don't intersect.
// Returns the segment's start-point if they are collinear.
func (l LineSegment2f) IntersectLine(other Line2f) (vmath.Vec2f, bool) {
	a := l.Start
	b := l.End
	c := other.Point

	r1a := (a[1] - c[1]) * other.Vec[0]
	r1b := (a[0] - c[0]) * other.Vec[1]
	r1 := r1a - r1b

	r2a := (b[0] - a[0]) * other.Vec[1]
	r2b := (b[1] - a[1]) * other.Vec[0]
	r2 := r2a - r2b

	if vmath.Equalf(r2a, r2b) { // parallel
		if vmath.Equalf(r1a, r1b) { // collinear
			// overlap. return any point on the line
			return l.Start, true
		}
		return vmath.Vec2f{}, false
	}

	r := r1 / r2
	if r < 0 || r > 1 { // no intersection on segment
		return vmath.Vec2f{}, false
	}

	return vmath.Vec2f{
		a[0] + r*(b[0]-a[0]),
		a[1] + r*(b[1]-a[1]),
	}, true
}

// X returns the line segment's x-coordinate at a given y-position.
// If the line segment does not cross this y-position, or if the segment is horizontal, false is returned.
func (l LineSegment2f) X(y float32) (float32, bool) {
	if vmath.Equalf(l.Start[1], l.End[1]) { // horizontal
		return 0, false
	}
	horizontal := Line2f{vmath.Vec2f{0, y}, vmath.Vec2f{1, 0}}
	origin, ok := l.IntersectLine(horizontal)
	return origin[0], ok
}

// Y returns the line segment's y-coordinate at a given x-position.
// If the line segment does not cross this x-position, or if the segment is vertical, false is returned.
func (l LineSegment2f) Y(x float32) (float32, bool) {
	if vmath.Equalf(l.Start[0], l.End[0]) { // vertical
		return 0, false
	}
	vertical := Line2f{vmath.Vec2f{x, 0}, vmath.Vec2f{0, 1}}
	origin, ok := l.IntersectLine(vertical)
	return origin[1], ok
}

// ClipX clips the line segment into a smaller one. This is done based on an interval on the x-axis.
// If the segment is already within the bounds, it is not modified.
// If the segment is completely outside the area, a segment with length zero is returned.
func (l LineSegment2f) ClipX(x1, x2 float32) LineSegment2f {
	if x2 < x1 {
		x1, x2 = x2, x1
	}
	startX := vmath.Clampf(l.Start[0], x1, x2)
	endX := vmath.Clampf(l.End[0], x1, x2)
	seg, _ := l.Line().ClipY(startX, endX)
	return seg
}

// ClipX clips the line segment into a smaller one. This is done based on an interval on the x-axis.
// If the segment is already within the bounds, it is not modified.
// If the segment is completely outside the area, a segment with length zero is returned.
func (l LineSegment2f) ClipY(y1, y2 float32) LineSegment2f {
	if y2 < y1 {
		y1, y2 = y2, y1
	}
	startY := vmath.Clampf(l.Start[1], y1, y2)
	endY := vmath.Clampf(l.End[1], y1, y2)
	seg, _ := l.Line().ClipY(startY, endY)
	return seg
}
