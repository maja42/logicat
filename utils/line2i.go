package utils

import (
	"fmt"

	"github.com/maja42/vmath"
)

// Line2i represents a line.
// The position and vector are represented as integers.
type Line2i struct {
	Point vmath.Vec2i
	Vec   vmath.Vec2i
}

func (l Line2i) String() string {
	return fmt.Sprintf("Line2i[%s -> %s]", l.Point.String(), l.Vec.String())
}

// Format the line segment to a string.
func (l Line2i) Format(segmentFormat, vecFormat string) string {
	return fmt.Sprintf(segmentFormat, l.Point.Format(vecFormat), l.Vec.Format(vecFormat))
}

// Line2f returns a float representation of the line.
func (l Line2i) Line2f() Line2f {
	return Line2f{
		Point: l.Point.Vec2f(),
		Vec:   l.Vec.Vec2f(),
	}
}

// IsOrthogonal returns true if the line is orthogonal (horizontal or vertical)
func (l Line2i) IsOrthogonal() bool {
	return l.Vec[0] == 0 || l.Vec[1] == 0
}

// IsPointCollinear checks if the given point is on the line.
func (l Line2i) IsPointCollinear(pos vmath.Vec2i) bool {
	pvec := pos.Sub(l.Point)
	return l.Vec.MagCross(pvec) == 0
}

// Equals checks if the two lines are equal.
// This is, the lines are collinear. Lines can be equal even if their points are different.
func (l Line2i) Equals(other Line2i) bool {
	return l.IsPointCollinear(other.Point) && l.Vec.IsParallel(other.Vec)
}

// Intersect returns the intersection point between two lines.
// Returns false if the lines are parallel and don't intersect.
// Returns any point on the line if they are equal (collinear).
func (l Line2i) Intersect(other Line2i) (vmath.Vec2f, bool) {
	a := l.Point
	b := a.Add(l.Vec)
	c := other.Point

	r1 := (a[1]-c[1])*other.Vec[0] - (a[0]-c[0])*other.Vec[1]
	r2 := l.Vec.MagCross(other.Vec)

	if r2 == 0 { // parallel
		if r1 == 0 { // collinear
			// overlap. return any point on the line
			return l.Point.Vec2f(), true
		}
		return vmath.Vec2f{}, false
	}

	r := float32(r1) / float32(r2)
	return vmath.Vec2f{
		float32(a[0]) + r*float32(b[0]-a[0]),
		float32(a[1]) + r*float32(b[1]-a[1]),
	}, true
}

// X returns the line's x-coordinate at a given y-position.
// If the line is horizontal, false is returned.
func (l Line2i) X(y int) (float32, bool) {
	if l.Vec[1] == 0 { // horizontal
		return 0, false
	}
	// TODO: Improve performance by not using intersect
	horizontal := Line2i{vmath.Vec2i{0, y}, vmath.Vec2i{1, 0}}
	origin, _ := horizontal.Intersect(l)
	return origin[0], true
}

// Y returns the line's y-coordinate at a given x-position.
// If the line is vertical, false is returned.
func (l Line2i) Y(x int) (float32, bool) {
	if l.Vec[0] == 0 { // vertical
		return 0, false
	}
	// TODO: Improve performance by not using intersect
	vertical := Line2i{vmath.Vec2i{x, 0}, vmath.Vec2i{0, 1}}
	origin, _ := vertical.Intersect(l)
	return origin[1], true
}

// Origin returns the y-position when x = 0; That's the intersection between the line and the y-axis.
// If the line is collinear to the x-axis, {0,0} and true is returned.
// If the line is vertical and does not intersect the y-axis, false is returned.
func (l Line2i) Origin() (float32, bool) {
	return l.Y(0)
}

// ClipX clips the line into a line segment. This is done based on an interval on the x-axis.
// If the line is vertical, false is returned.
func (l Line2i) ClipX(x1, x2 int) (LineSegment2f, bool) {
	y1, ok := l.Y(x1)
	if !ok { // vertical
		return LineSegment2f{}, false
	}

	y2, _ := l.Y(x2)

	return LineSegment2f{
		Start: vmath.Vec2f{float32(x1), y1},
		End:   vmath.Vec2f{float32(x2), y2},
	}, true
}

// ClipY clips the line into a line segment. This is done based on an interval on the y-axis.
// If the line is horizontal, false is returned.
func (l Line2i) ClipY(y1, y2 int) (LineSegment2f, bool) {
	x1, ok := l.X(y1)
	if !ok { // horizontal
		return LineSegment2f{}, false
	}

	x2, _ := l.Y(y2)

	return LineSegment2f{
		Start: vmath.Vec2f{x1, float32(y1)},
		End:   vmath.Vec2f{x2, float32(y2)},
	}, true
}
