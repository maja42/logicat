package utils

import "C"
import (
	"fmt"

	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
	"github.com/maja42/vmath/mathi"
)

// LineSegment2i represents a line segment.
// Both positions are defined with integers.
type LineSegment2i struct {
	Start vmath.Vec2i
	End   vmath.Vec2i
}

func (l LineSegment2i) String() string {
	return fmt.Sprintf("Seg2i[%s -> %s]", l.Start.String(), l.End.String())
}

// Format the line segment to a string.
func (l LineSegment2i) Format(segmentFormat, vecFormat string) string {
	return fmt.Sprintf(segmentFormat, l.Start.Format(vecFormat), l.End.Format(vecFormat))
}

// LineSegment2f returns a float representation of the line segment.
func (l LineSegment2i) LineSegment2f() LineSegment2f {
	return LineSegment2f{
		Start: l.Start.Vec2f(),
		End:   l.End.Vec2f(),
	}
}

// Line converts the line segment into a line.
func (l LineSegment2i) Line() Line2i {
	return Line2i{
		Point: l.Start,
		Vec:   l.Vec(),
	}
}

// Vec returns a vector from the start-point to the end-point of the segment.
func (l LineSegment2i) Vec() vmath.Vec2i {
	return l.End.Sub(l.Start)
}

// Length returns the segment's length.
func (l LineSegment2i) Length() float32 {
	return l.Vec().Length()
}

// SquareLength returns the segment's squared length.
func (l LineSegment2i) SquareLength() int {
	return l.Vec().SquareLength()
}

// HasZeroLength returns true if the segment has a length of zero.
func (l LineSegment2i) HasZeroLength() bool {
	return l.Start == l.End
}

// IsHorizontal returns true if the line is horizontal (same y coordinate)
func (l LineSegment2i) IsHorizontal() bool {
	return l.Start[1] == l.End[1]
}

// IsVertical returns true if the line is horizontal (same x coordinate)
func (l LineSegment2i) IsVertical() bool {
	return l.Start[0] == l.End[0]
}

// Project projects the line segment onto a given vector/axis.
func (l LineSegment2i) Project(v vmath.Vec2f) LineSegment2f {
	return LineSegment2f{
		Start: l.Start.Vec2f().Project(v),
		End:   l.End.Vec2f().Project(v),
	}
}

// IsPointCollinear checks if the given point is on the same line as this segment.
// "Line" is defined as expanding indefinitely on both sides of the segment.
func (l LineSegment2i) IsPointCollinear(pos vmath.Vec2i) bool {
	lvec := l.Vec()
	pvec := pos.Sub(l.Start)
	return lvec.MagCross(pvec) == 0
}

// IsLineSegmentCollinear checks if the two segments are on the same line.
// "Line" is defined as expanding indefinitely on both sides of the segment.
// Lines that are collinear are also parallel.
func (l LineSegment2i) IsLineSegmentCollinear(other LineSegment2i) bool {
	return l.IsPointCollinear(other.Start) && l.IsPointCollinear(other.End)
}

// Contains checks if the given position is on the line segment.
func (l LineSegment2i) Contains(pos vmath.Vec2i) bool {
	lvec := l.Vec()
	pvec := pos.Sub(l.Start)
	if lvec.MagCross(pvec) != 0 { // Not collinear
		return false
	}
	if lvec.Dot(pvec) < 0 { // point is behind line segment
		return false
	}
	if lvec.SquareLength() < pvec.SquareLength() {
		return false
	}
	return true
}

// Intersect returns the intersection point between two line segments or false if they don't intersect.
// Returns the point with the lowest x-position if the segments overlap.
func (l LineSegment2i) Intersect(other LineSegment2i) (vmath.Vec2f, bool) {
	// Source: https://gamedev.stackexchange.com/a/117294/39091

	a := l.Start
	b := l.End
	c := other.Start
	d := other.End

	r1 := (a[1]-c[1])*(d[0]-c[0]) - (a[0]-c[0])*(d[1]-c[1])
	r2 := (b[0]-a[0])*(d[1]-c[1]) - (b[1]-a[1])*(d[0]-c[0])
	if r2 == 0 { // parallel
		if r1 == 0 { // collinear
			// Check overlap. Project both segments onto x-axis:
			a := l.Project(vmath.Vec2f{1, 0})
			b := other.Project(vmath.Vec2f{1, 0})
			if b.Start[0] <= a.End[0] || b.End[1] >= a.Start[0] { // overlap. Return the intersection point with the lowest X
				if a.Start[0] < b.Start[0] {
					return b.Start, true
				} else {
					return a.Start, true
				}
			}
			return vmath.Vec2f{}, false
		}
		return vmath.Vec2f{}, false
	}

	r := float32(r1) / float32(r2)
	if r < 0 || r > 1 { // no intersection on segment "l"
		return vmath.Vec2f{}, false
	}

	s1 := (a[1]-c[1])*(b[0]-a[0]) - (a[0]-c[0])*(b[1]-a[1])
	s2 := r2
	s := float32(s1) / float32(s2)
	if s < 0 || s > 1 { // no intersection on segment "other"
		return vmath.Vec2f{}, false
	}

	return vmath.Vec2f{
		float32(a[0]) + r*float32(b[0]-a[0]),
		float32(a[1]) + r*float32(b[1]-a[1]),
	}, true
}

// IntersectLine returns the intersection point between the line segment and the given line.
// Returns false if they don't intersect.
// Returns the segment's start-point if they are collinear.
func (l LineSegment2i) IntersectLine(other Line2i) (vmath.Vec2f, bool) {
	a := l.Start
	b := l.End
	c := other.Point

	r1 := (a[1]-c[1])*other.Vec[0] - (a[0]-c[0])*other.Vec[1]
	r2 := (b[0]-a[0])*other.Vec[1] - (b[1]-a[1])*other.Vec[0]

	if r2 == 0 { // parallel
		if r1 == 0 { // collinear
			// overlap. return any point on the line
			return l.Start.Vec2f(), true
		}
		return vmath.Vec2f{}, false
	}

	r := float32(r1) / float32(r2)
	if r < 0 || r > 1 { // no intersection on segment
		return vmath.Vec2f{}, false
	}

	return vmath.Vec2f{
		float32(a[0]) + r*float32(b[0]-a[0]),
		float32(a[1]) + r*float32(b[1]-a[1]),
	}, true
}

//// X returns the line segment's x-coordinate at a given y-position.
//// If the line segment does not cross this y-position, or if the segment is horizontal, false is returned.
//func (l LineSegment2i) X(y int) (float32, bool) {
//	if l.Start[1] == l.End[1] { // horizontal
//		return 0, false
//	}
//	horizontal := Line2i{vmath.Vec2i{0, y}, vmath.Vec2i{1, 0}}
//	origin, ok := l.IntersectLine(horizontal)
//	return origin[0], ok
//}
//
//// Y returns the line segment's y-coordinate at a given x-position.
//// If the line segment does not cross this x-position, or if the segment is vertical, false is returned.
//func (l LineSegment2i) Y(x int) (float32, bool) {
//	if l.Start[0] == l.End[0] { // vertical
//		return 0, false
//	}
//	vertical := Line2i{vmath.Vec2i{x, 0}, vmath.Vec2i{0, 1}}
//	origin, ok := l.IntersectLine(vertical)
//	return origin[1], ok
//}

// PointsOnSegment returns all positions touched by the line segment.
// Only supports orthogonal segments and segments at 45°/135°.
func PointsOnSegment(seg LineSegment2i) []vmath.Vec2i {
	if seg.Start == seg.End {
		return []vmath.Vec2i{seg.Start}
	}

	var step vmath.Vec2i
	var points int

	for dim := 0; dim < 2; dim++ {
		a, b := seg.Start[dim], seg.End[dim]
		if a != b {
			points = mathi.Abs(b-a) + 1
			if a < b {
				step[dim] = 1
			} else {
				step[dim] = -1
			}
		}
	}
	p := make([]vmath.Vec2i, points)
	p[0] = seg.Start
	for i := 1; i < points; i++ {
		p[i] = p[i-1].Add(step)
	}
	assert.True(points >= 2 && p[points-1] == seg.End, "wrong segment point calculation")
	return p
}

// SegmentOverlap checks if two line segments are overlapping.
// If the segments only touch or cross each other, they do not overlap.
func SegmentOverlap(a, b LineSegment2i) bool {
	if !a.IsLineSegmentCollinear(b) {
		return false
	}

	avec := a.Vec()
	bvec := b.Vec()

	if avec.Dot(bvec) < 0 { // opposite direction
		a.Start, a.End = a.End, a.Start
		avec = avec.Negate()
	}

	aToBVec := b.Start.Sub(a.Start)
	if aToBVec.Dot(avec) >= 0 { // [-90, +90°] --> b starts after a
		if aToBVec.SquareLength() < avec.SquareLength() {
			return true // b starts in the middle of a
		}
	} else { // b starts before a. Does it also end before a?
		if aToBVec.SquareLength() < bvec.SquareLength() {
			return true // b ends in the middle of (or after) a
		}
	}
	return false
}
