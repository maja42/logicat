package utils

import (
	"testing"

	"github.com/maja42/vmath"
)

func segment(from, to int) LineSegment2i {
	return LineSegment2i{
		Start: vmath.Vec2i{from, 0},
		End:   vmath.Vec2i{to, 0},
	}
}

func segmentY(from, to int, y int) LineSegment2i {
	return LineSegment2i{
		Start: vmath.Vec2i{from, y},
		End:   vmath.Vec2i{to, y},
	}
}

func TestSegmentOverlap(t *testing.T) {
	tests := map[string]struct {
		a    LineSegment2i
		b    LineSegment2i
		want bool
	}{
		"a equal b": {
			segment(0, 10),
			segment(0, 10),
			true,
		},
		"a exceeds b": {
			segment(0, 15),
			segment(0, 10),
			true,
		},
		"a precedes b": {
			segment(-5, 10),
			segment(0, 10),
			true,
		},

		"b starts in a": {
			segment(0, 10),
			segment(5, 15),
			true,
		},
		"b within a": {
			segment(0, 10),
			segment(4, 7),
			true,
		},
		"b ends in a": {
			segment(0, 10),
			segment(-5, 5),
			true,
		},

		"b before a (touch)": {
			segment(0, 10),
			segment(-5, 0),
			false,
		},

		"b before a": {
			segment(0, 10),
			segment(-10, -5),
			false,
		},
		"b after a (touch)": {
			segment(0, 10),
			segment(10, 15),
			false,
		},
		"b after a": {
			segment(0, 10),
			segment(15, 20),
			false,
		},
		//
		"a parallel b": {
			segmentY(0, 10, 0),
			segmentY(0, 10, 5),
			false,
		},
		"a crosses b": {
			LineSegment2i{
				Start: vmath.Vec2i{0, 0},
				End:   vmath.Vec2i{10, 0},
			},
			LineSegment2i{
				Start: vmath.Vec2i{5, -10},
				End:   vmath.Vec2i{5, 10},
			},
			false,
		},
	}
	for name, testCase := range tests {
		t.Run(name, func(t *testing.T) {
			if got := SegmentOverlap(testCase.a, testCase.b); got != testCase.want {
				t.Errorf("SegmentOverlap(a/b) = %v, want %v", got, testCase.want)
			}
			if got := SegmentOverlap(testCase.b, testCase.a); got != testCase.want {
				t.Errorf("SegmentOverlap(b/a) = %v, want %v", got, testCase.want)
			}

			// Invert A:
			testCase.a.Start, testCase.a.End = testCase.a.End, testCase.a.Start
			if got := SegmentOverlap(testCase.a, testCase.b); got != testCase.want {
				t.Errorf("SegmentOverlap(a/b) = %v, want %v", got, testCase.want)
			}
			if got := SegmentOverlap(testCase.b, testCase.a); got != testCase.want {
				t.Errorf("SegmentOverlap(b/a) = %v, want %v", got, testCase.want)
			}

			// Invert A + B:
			testCase.b.Start, testCase.b.End = testCase.b.End, testCase.b.Start
			if got := SegmentOverlap(testCase.a, testCase.b); got != testCase.want {
				t.Errorf("SegmentOverlap(a/b) = %v, want %v", got, testCase.want)
			}
			if got := SegmentOverlap(testCase.b, testCase.a); got != testCase.want {
				t.Errorf("SegmentOverlap(b/a) = %v, want %v", got, testCase.want)
			}

			// Invert B:
			testCase.a.Start, testCase.a.End = testCase.a.End, testCase.a.Start
			if got := SegmentOverlap(testCase.a, testCase.b); got != testCase.want {
				t.Errorf("SegmentOverlap(a/b) = %v, want %v", got, testCase.want)
			}
			if got := SegmentOverlap(testCase.b, testCase.a); got != testCase.want {
				t.Errorf("SegmentOverlap(b/a) = %v, want %v", got, testCase.want)
			}
		})
	}
}
