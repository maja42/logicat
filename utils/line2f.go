package utils

import (
	"fmt"

	"github.com/maja42/vmath"
)

// Line2f represents a line.
// The position and vector are represented as floats.
type Line2f struct {
	Point vmath.Vec2f
	Vec   vmath.Vec2f
}

func (l Line2f) String() string {
	return fmt.Sprintf("Line2f[%s -> %s]", l.Point.String(), l.Vec.String())
}

// Format the line segment to a string.
func (l Line2f) Format(segmentFormat, vecFormat string) string {
	return fmt.Sprintf(segmentFormat, l.Point.Format(vecFormat), l.Vec.Format(vecFormat))
}

// Line2f returns an integer representation of the line.
// Decimals are truncated.
func (l Line2f) Line2i() Line2i {
	return Line2i{
		Point: l.Point.Vec2i(),
		Vec:   l.Vec.Vec2i(),
	}
}

// Line2f returns an integer representation of the line.
// Decimals are rounded.
func (l Line2f) Round() Line2i {
	return Line2i{
		Point: l.Point.Round(),
		Vec:   l.Vec.Round(),
	}
}

// IsOrthogonal returns true if the line is orthogonal (horizontal or vertical).
// Uses the default Epsilon as relative tolerance.
func (l Line2f) IsOrthogonal() bool {
	return vmath.Equalf(l.Vec[0], 0) || vmath.Equalf(l.Vec[1], 0)
}

// IsPointCollinear checks if the given point is on the line.
// Uses the default Epsilon as relative tolerance.
func (l Line2f) IsPointCollinear(pos vmath.Vec2f) bool {
	pvec := pos.Sub(l.Point)
	return vmath.Equalf(l.Vec[0]*pvec[1], l.Vec[1]*pvec[0]) // l.Vec.MagCross(pvec) == 0
}

// Equals checks if the two lines are equal.
// This is, the lines are collinear. Lines can be equal even if their points are different.
// Uses the default Epsilon as relative tolerance.
func (l Line2f) Equals(other Line2f) bool {
	return l.IsPointCollinear(other.Point) && l.Vec.IsParallel(other.Vec)
}

// Intersect returns the intersection point between two lines.
// Returns false if the lines are parallel and don't intersect.
// Returns any point on the line if they are equal (collinear).
// Uses the default Epsilon as relative tolerance.
func (l Line2f) Intersect(other Line2f) (vmath.Vec2f, bool) {
	a := l.Point
	b := a.Add(l.Vec)
	c := other.Point

	r1a := (a[1] - c[1]) * other.Vec[0]
	r1b := (a[0] - c[0]) * other.Vec[1]
	r1 := r1a - r1b
	r2a := l.Vec[0] * other.Vec[1]
	r2b := l.Vec[1] * other.Vec[0]
	r2 := r2a - r2b // l.Vec.MagCross(other.Vec)

	if vmath.Equalf(r1a, r1b) { // parallel
		if vmath.Equalf(r2a, r2b) { // collinear
			// overlap. return any point on the line
			return l.Point, true
		}
		return vmath.Vec2f{}, false
	}

	r := r1 / r2
	return vmath.Vec2f{
		a[0] + r*(b[0]-a[0]),
		a[1] + r*(b[1]-a[1]),
	}, true
}

// X returns the line's x-coordinate at a given y-position.
// If the line is horizontal, false is returned.
func (l Line2f) X(y float32) (float32, bool) {
	if l.Vec[1] == 0 { // horizontal
		return 0, false
	}
	// TODO: Improve performance by not using intersect
	horizontal := Line2f{vmath.Vec2f{0, y}, vmath.Vec2f{1, 0}}
	origin, _ := horizontal.Intersect(l)
	return origin[0], true
}

// Y returns the line's y-coordinate at a given x-position.
// If the line is vertical, false is returned.
func (l Line2f) Y(x float32) (float32, bool) {
	if l.Vec[0] == 0 { // vertical
		return 0, false
	}
	// TODO: Improve performance by not using intersect
	vertical := Line2f{vmath.Vec2f{x, 0}, vmath.Vec2f{0, 1}}
	origin, _ := vertical.Intersect(l)
	return origin[1], true
}

// Origin returns the y-position when x = 0; That's the intersection between the line and the y-axis.
// If the line is collinear to the x-axis, {0,0} and true is returned.
// If the line is vertical and does not intersect the y-axis, false is returned.
func (l Line2f) Origin() (float32, bool) {
	return l.Y(0)
}

// ClipX clips the line into a line segment. This is done based on an interval on the x-axis.
// The direction of the line segment is x1-->x2.
// If the line is vertical, false is returned.
func (l Line2f) ClipX(x1, x2 float32) (LineSegment2f, bool) {
	y1, ok := l.Y(x1)
	if !ok { // vertical
		return LineSegment2f{}, false
	}

	y2, _ := l.Y(x2)

	return LineSegment2f{
		Start: vmath.Vec2f{x1, y1},
		End:   vmath.Vec2f{x2, y2},
	}, true
}

// ClipY clips the line into a line segment. This is done based on an interval on the y-axis.
// The direction of the line segment is y1-->y2.
// If the line is horizontal, false is returned.
func (l Line2f) ClipY(y1, y2 float32) (LineSegment2f, bool) {
	x1, ok := l.X(y1)
	if !ok { // horizontal
		return LineSegment2f{}, false
	}

	x2, _ := l.Y(y2)

	return LineSegment2f{
		Start: vmath.Vec2f{x1, y1},
		End:   vmath.Vec2f{x2, y2},
	}, true
}
