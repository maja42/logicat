package utils

import (
	"math/rand"

	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

// RandomColor returns a random color with a high saturation to be easily distinguishable.
func RandomColor() color.Color {
	return color.HSL(rand.Float32(), 1, 0.5)
}

func Vec2iSliceContainsDuplicates(v []vmath.Vec2i) bool {
	m := make(map[vmath.Vec2i]struct{}, len(v))
	for _, vec := range v {
		if _, ok := m[vec]; ok {
			return true
		}
		m[vec] = struct{}{}
	}
	return false
}

// UniqueVec2iSlice removes all duplicates from the given input slice
func UniqueVec2iSlice(v []vmath.Vec2i) []vmath.Vec2i {
	m := make(map[vmath.Vec2i]struct{}, len(v))
	for _, vec := range v {
		m[vec] = struct{}{}
	}
	uniq := make([]vmath.Vec2i, len(m))
	idx := 0
	for vec := range m {
		uniq[idx] = vec
		idx++
	}
	return uniq
}

func Vec2iSliceContains(v []vmath.Vec2i, s vmath.Vec2i) bool {
	for _, vec := range v {
		if vec == s {
			return true
		}
	}
	return false
}

// ReverseVec2iSlice reverses all positions in the given array
func ReverseVec2iSlice(v []vmath.Vec2i) {
	left := 0
	right := len(v) - 1
	for ; left < right; left, right = left+1, right-1 {
		v[left], v[right] = v[right], v[left]
	}
}
