package utils

import (
	"testing"

	"github.com/maja42/vmath"
	"github.com/stretchr/testify/assert"
)

func TestUniqueVec2iSlice(t *testing.T) {
	orig := []vmath.Vec2i{
		{0, 4},
		{2, 4},
		{4, 3},
		{4, 3},
		{8, 8},
		{2, 4},
		{4, 3},
	}
	in := make([]vmath.Vec2i, len(orig))
	copy(in, orig)

	uniq := UniqueVec2iSlice(in)
	assert.Len(t, uniq, 4)
	assert.Equal(t, orig, in)
}
