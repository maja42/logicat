package pool

import (
	"github.com/maja42/nora/assert"
	"github.com/sirupsen/logrus"
)

type Poolable interface {
	Destroy()
}

type NewFunc func() Poolable
type ResetFunc func(Poolable)

// Pool is a generic implementation that supplies objects.
// It's used to avoid the destruction and creation of objects with a short lifespan that could be reused.
//
// Unneeded objects should be returned to the pool.
// Retrieved objects are reset and therefore clean.
// Objects are destroyed when unneeded.
// The pool guarantees that it does not grow indefinitely.
// Do not use concurrently.
type Pool struct {
	// cannot use sync.pool, because it removes objects in the background without freeing/destroying them (and eventual GPU objects behind it)
	objects []Poolable
	maxSize int
	new     NewFunc
	reset   ResetFunc
}

// NewPool creates a new pool. Optionally pre-allocates the pool to reach the initial size.
func NewPool(initialSize, maxSize int, newFunc NewFunc, resetFunc ResetFunc) *Pool {
	pool := &Pool{
		objects: make([]Poolable, 0, maxSize),
		maxSize: maxSize,
		new:     newFunc,
		reset:   resetFunc,
	}
	pool.Fill(initialSize)
	return pool
}

// Fill the pool up to a given level.
func (p *Pool) Fill(size int) {
	assert.True(size <= p.maxSize, "cannot grow pool larger than it's maximum")
	for i := 0; i < size; i++ {
		p.objects = append(p.objects, p.new())
	}
}

// Get an element from the pool.
// If the pool is empty, a new object is created.
// The element will be reset and is therefore clean.
func (p *Pool) Get() Poolable {
	if len(p.objects) == 0 {
		return p.new()
	}
	lastIdx := len(p.objects) - 1
	lastObj := p.objects[lastIdx]
	p.objects = p.objects[:lastIdx]

	p.reset(lastObj)
	return lastObj
}

// Put puts the object into the pool.
// If the pool is full, the object is destroyed.
func (p *Pool) Put(object Poolable) {
	assert.True(object != nil, "cannot return nil to pool")

	if len(p.objects) >= p.maxSize {
		logrus.Debugf("Pool full: destroying object")
		object.Destroy()
		return
	}
	p.objects = append(p.objects, object)
}

// Destroy the pool and all pooled objects.
// The pool is unusable afterwards.
func (p *Pool) Destroy() {
	for _, obj := range p.objects {
		obj.Destroy()
	}
	p.objects = nil
	p.maxSize = 0
	p.new = nil
	p.reset = nil
}
