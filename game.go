package main

import (
	"time"

	"github.com/maja42/logicat/action"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/logicat/theme"
	"github.com/sirupsen/logrus"

	"github.com/maja42/glfw"
	"github.com/maja42/nora"
	"github.com/maja42/vmath"
)

const saveFile = "scene.json"

type GameStateMode int

const (
	GameStateSelectionMode = GameStateMode(iota)
	GameStatePlaceComponents
	GameStatePlaceWire
)

//func (s GameStateMode) String() string {
//	switch s {
//	case GameStateSelectionMode:
//		return "selection mode"
//	case GameStatePlaceComponents:
//		return "place components"
//	case GameStatePlaceWire:
//		return "place wire"
//	}
//	return fmt.Sprintf("unknown(%d)", s)
//}

type GameState interface {
	String() string

	Update()
	Draw(*nora.RenderState)

	Destroy()
}

type Game struct {
	engine    *nora.Engine
	cam       *nora.OrthoCamera
	gamePanel *GamePanel

	domain *logic.Domain

	schematic *Schematic
	mpanel    *action.Panel

	gameMode GameStateMode
	state    GameState

	camMovement *CameraMovement

	fonts []*nora.Font

	keyEventCBID nora.CallbackID
	saveKeyCBID  nora.CallbackID
	loadKeyCBID  nora.CallbackID
	resetKeyCBID nora.CallbackID
}

func NewGame(engine *nora.Engine) *Game {
	g := &Game{
		engine: engine,
		cam:    engine.Camera.(*nora.OrthoCamera),
	}
	dbgPanelFont, err := nora.LoadFont(noraBuiltin + "/fonts/ibm plex mono/ibm_plex_mono_regular_32.xml")
	if err != nil {
		logrus.Errorf("Failed to load font: %s", err)
	}
	theme.LampValueTextFont = dbgPanelFont

	gateFont, err := nora.LoadFont(noraBuiltin + "/fonts/roboto/roboto_regular_65.xml")
	//gateFont, err := nora.LoadFont(noraBuiltin + "/fonts/ibm plex mono/ibm_plex_mono_light_65.xml")
	if err != nil {
		logrus.Errorf("Failed to load font: %s", err)
	}
	theme.GateLabelFont = gateFont

	g.fonts = append(g.fonts, dbgPanelFont, gateFont)

	g.domain = logic.NewDomain()

	g.gamePanel = NewGamePanel(engine, dbgPanelFont, g)

	g.schematic = NewSchematic(g.domain, engine, g.gamePanel, vmath.Vec2i{100, 80})
	g.mpanel = action.NewPanel(&engine.InteractionSystem, func(clipSpacePos vmath.Vec2f) vmath.Vec2f {
		world := engine.Camera.(*nora.OrthoCamera).ClipSpaceToWorldSpace(clipSpacePos)
		schemaCoord, _ := g.schematic.SchemaCoord(world)
		return schemaCoord
	})

	g.camMovement = NewCameraMovement(g.cam, &engine.InteractionSystem)

	g.gameMode = GameStateSelectionMode
	g.state = NewSelector(&engine.InteractionSystem, g.schematic, g.schematic.Networking)

	g.keyEventCBID = engine.InteractionSystem.OnKeyEvent(g.onKeyEvent)

	g.saveKeyCBID = engine.InteractionSystem.OnKey(glfw.KeyF5, glfw.Press, g.onSaveKey)
	g.loadKeyCBID = engine.InteractionSystem.OnKey(glfw.KeyF6, glfw.Press, g.onLoadKey)
	g.resetKeyCBID = engine.InteractionSystem.OnKey(glfw.KeyF7, glfw.Press, g.onResetKey)
	return g
}

func (g *Game) onKeyEvent(key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	if action != glfw.Release {
		return
	}
	switch key {
	case glfw.KeyS:
		g.switchGameMode(GameStateSelectionMode)
	case glfw.KeyC:
		g.switchGameMode(GameStatePlaceComponents)
	case glfw.KeyW:
		g.switchGameMode(GameStatePlaceWire)
	}
}

func (g *Game) switchGameMode(newMode GameStateMode) {
	if g.state != nil && g.gameMode == newMode {
		return
	}
	if g.state != nil {
		g.state.Destroy()
	}

	switch newMode {
	case GameStatePlaceComponents:
		logrus.Infof("Switched game mode: component placement")
		g.state = NewPlacer(&g.engine.InteractionSystem, g.mpanel, g.schematic, g.domain.NetValue)
	case GameStatePlaceWire:
		logrus.Infof("Switched game mode: wire placement")
		g.state = NewWireTool(&g.engine.InteractionSystem, g.schematic, g.schematic.Networking, g.gamePanel)
	case GameStateSelectionMode:
		fallthrough
	default:
		logrus.Infof("Switched game mode: selection")
		g.state = NewSelector(&g.engine.InteractionSystem, g.schematic, g.schematic.Networking)
	}
	g.gameMode = newMode
}

func (g *Game) onSaveKey(modKey glfw.ModifierKey) {
	logrus.Infof("Saving current scene...")
	if err := g.schematic.ExportToFile(saveFile); err != nil {
		logrus.Errorf("Failed to save scene: %v", err)
	}
	logrus.Infof("Saved as %s", saveFile)
}

func (g *Game) onLoadKey(modKey glfw.ModifierKey) {
	logrus.Infof("Loading current scene...")
	schematic, err := ImportSchematicFromFile(g.domain, g.engine, g.gamePanel, saveFile)
	if err != nil {
		logrus.Errorf("Failed to import scene: %v", err)
	}
	g.state.Destroy()
	g.state = nil
	g.schematic.Destroy()
	g.schematic = schematic
	g.switchGameMode(GameStateSelectionMode)
}

func (g *Game) onResetKey(modKey glfw.ModifierKey) {
	g.switchGameMode(GameStateSelectionMode)
	g.schematic.Clear()
}

func (g *Game) Frame(elapsed time.Duration, renderState *nora.RenderState) {
	g.schematic.Update(elapsed)
	g.state.Update()

	g.domain.Tick() // TODO: use nora.FixedUpdate

	g.schematic.Draw(renderState)

	g.state.Draw(renderState)

	g.gamePanel.Update()
	g.gamePanel.Draw(renderState)

}

func (g *Game) Destroy() {
	interactionSys := &g.engine.InteractionSystem
	interactionSys.RemoveKeyEventFunc(g.keyEventCBID)
	interactionSys.RemoveKeyEventFunc(g.saveKeyCBID)
	interactionSys.RemoveKeyEventFunc(g.loadKeyCBID)
	interactionSys.RemoveKeyEventFunc(g.resetKeyCBID)

	for _, font := range g.fonts {
		font.Destroy()
	}
	theme.GateLabelFont = nil

	g.state.Destroy()
	g.camMovement.Destroy()
	g.schematic.Destroy()
	g.gamePanel.Destroy()
}
