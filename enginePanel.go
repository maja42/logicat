package main

import (
	"runtime"
	"time"

	"github.com/maja42/glfw"
	"github.com/maja42/logicat/dbgpanel"
	"github.com/maja42/nora"
)

type EnginePanel struct {
	debugPanel dbgpanel.DebugPanel
	engine     *nora.Engine

	lastMemStatUpdate time.Time
	memStats          runtime.MemStats
	keyCBID           nora.CallbackID
}

func NewEnginePanel(engine *nora.Engine, font *nora.Font) *EnginePanel {
	d := &EnginePanel{
		debugPanel: *dbgpanel.NewDebugPanel(font, dbgpanel.BottomRightCorner, 13),
		engine:     engine,
	}
	d.keyCBID = engine.InteractionSystem.OnKey(glfw.KeyF1, glfw.Press, d.onF1)
	return d
}

func (d *EnginePanel) onF1(glfw.ModifierKey) {
	d.debugPanel.ToggleVisibility()
}

func (d *EnginePanel) Update() {
	windowSize := d.engine.InteractionSystem.WindowSize()
	mousePos := d.engine.InteractionSystem.MousePosWindowSpace()
	clipSpace := d.engine.InteractionSystem.MousePosClipSpace()

	cam := d.engine.Camera.(*nora.OrthoCamera)
	worldSpace := cam.ClipSpaceToWorldSpace(clipSpace)

	panel := &d.debugPanel

	panel.Section("mouse position").
		Set("window space", mousePos.Format("[%7d x %7d]")).
		Set("clip space", clipSpace.Format("[%7.2f x %7.2f]")).
		Set("world space", worldSpace.Format("[%7.2f x %7.2f]"))

	panel.Section("camera").
		Set("world space", cam.Position().Format("[%7.2f x %7.2f]")).
		Set("ortho size", cam.OrthoSize().Format("[%7.2f x %7.2f]"))

	rendStats := d.engine.RenderStats()

	panel.Section("scene").
		Setf("shader progs", "%d", d.engine.Shaders.Count()).
		Setf("draw calls", "%d", rendStats.TotalDrawCalls).
		Setf("primitives", "%d", rendStats.TotalPrimitives)

	panel.Section("interaction callbacks").
		Setf("key", "%d", d.engine.InteractionSystem.KeyCallbackCount()).
		Setf("mouse button", "%d", d.engine.InteractionSystem.MouseButtonCallbackCount()).
		Setf("mouse move", "%d", d.engine.InteractionSystem.MouseMoveCallbackCount()).
		Setf("scroll", "%d", d.engine.InteractionSystem.ScrollCallbackCount())

	panel.Section("rendering").
		Set("resolution", windowSize.Format("[%7d x %7d]")).
		Setf("frame", "%d", rendStats.Frame).
		Setf("frame rate", "%.2f fps", rendStats.Framerate)

	//if time.Since(d.lastMemStatUpdate) > 500*time.Millisecond {
	//	// reading memory statistics is an expensive runtime call that stops the world and iterates the whole heap
	//	runtime.ReadMemStats(&d.memStats)
	//	d.lastMemStatUpdate = time.Now()
	//}
	//panel.Section("go runtime").
	//	Set("go routines", fmt.Sprintf("%d", runtime.NumGoroutine())).
	//	Set("cgo calls", fmt.Sprintf("%d", runtime.NumCgoCall())).
	//	Set("heap", fmt.Sprintf("%.1fMB", float32(d.memStats.HeapAlloc)/1024/1024)).
	//	Set("allocs", fmt.Sprintf("%d", d.memStats.Mallocs-d.memStats.Frees)).
	//	Set("gc cycles", fmt.Sprintf("%d", d.memStats.NumGC)).
	//	Set("gc CPU time", fmt.Sprintf("%.3f%%", d.memStats.GCCPUFraction*100))
}

func (d *EnginePanel) Destroy() {
	d.engine.InteractionSystem.RemoveKeyEventFunc(d.keyCBID)
	d.debugPanel.Destroy()
}

func (m *EnginePanel) Draw(renderState *nora.RenderState) {
	m.debugPanel.Draw(m.engine.Camera.(*nora.OrthoCamera), renderState)
}
