package dbgpanel

import (
	"fmt"
	"strconv"

	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/shapes"
	"github.com/maja42/vmath"
	"github.com/maja42/vmath/mathi"
)

type Location int

const (
	TopLeftCorner = Location(iota)
	TopRightCorner
	BottomLeftCorner
	BottomRightCorner
)

// DebugPanel is used to show textual debugging information in a screen-corner to the user
type DebugPanel struct {
	transform nora.Transform // private

	font     *nora.Font
	location Location
	fontSize float32

	sections []*Section

	text      *shapes.Text
	textDirty bool

	display bool
}

type Section struct {
	panel   *DebugPanel
	name    string
	entries []entry
}

type entry struct {
	key   string
	value string
}

// NewDebugPanel creates a new debug panel at the given location.
// Requires a monospace font.
func NewDebugPanel(font *nora.Font, location Location, fontSize float32) *DebugPanel {
	assert.True(font.Monospace, "debug panel requires monospace font")
	d := &DebugPanel{
		font:     font,
		location: location,
		fontSize: fontSize,

		sections:  make([]*Section, 0),
		text:      shapes.NewText(font, ""),
		textDirty: false,

		display: true,
	}
	return d
}

func (d *DebugPanel) ToggleVisibility() {
	d.display = !d.display
}

func (d *DebugPanel) Section(sectionName string) *Section {
	for _, section := range d.sections {
		if section.name == sectionName {
			return section
		}
	}
	section := &Section{
		panel:   d,
		name:    sectionName,
		entries: make([]entry, 0),
	}
	d.sections = append(d.sections, section)
	return section
}

func (d *DebugPanel) RemoveSection(sectionName string) *DebugPanel {
	for idx, section := range d.sections {
		if section.name == sectionName {
			d.sections = append(d.sections[:idx], d.sections[idx+1:]...)
		}
	}
	d.textDirty = true
	return d
}

func (d *DebugPanel) Clear() {
	d.sections = make([]*Section, 0)
}

func (s *Section) Set(key, value string) *Section {
	s.panel.textDirty = true

	for idx, entry := range s.entries {
		if entry.key == key {
			s.entries[idx].value = value
			return s
		}
	}
	s.entries = append(s.entries, entry{
		key:   key,
		value: value,
	})
	return s
}

func (s *Section) Setf(key string, format string, a ...interface{}) *Section {
	return s.Set(key, fmt.Sprintf(format, a...))
}

func (s *Section) Remove(key string) *Section {
	for idx, entry := range s.entries {
		if entry.key == key {
			s.entries = append(s.entries[:idx], s.entries[idx+1:]...)
			s.panel.textDirty = true
		}
	}
	return s
}

func (s *Section) Clear() *Section {
	s.entries = s.entries[:0]
	return s
}

func (d *DebugPanel) updateText() {
	maxKeyLength := 0
	for _, section := range d.sections {
		for _, entry := range section.entries {
			maxKeyLength = mathi.Max(maxKeyLength, len(entry.key))
		}
	}

	fmtStr := "  %-" + strconv.Itoa(maxKeyLength) + "s %s\n"
	txt := ""
	for _, section := range d.sections {
		txt += section.name + "\n"
		for _, entry := range section.entries {
			txt += fmt.Sprintf(fmtStr, entry.key, entry.value)
		}
	}
	txt = txt[:len(txt)-1] // remove last newline

	d.text.Set(txt)
}

func (d *DebugPanel) updatePosition(cam *nora.OrthoCamera) {
	scale := d.fontSize / 1000 // get into a useful value range
	scale *= cam.OrthoWidth()

	d.transform.SetUniformScale(scale)

	bounds := d.text.Bounds()
	tl := vmath.Vec2f{cam.Left() - bounds.Min[0]*scale, cam.Top() - bounds.Max[1]*scale}
	br := vmath.Vec2f{cam.Right() - bounds.Max[0]*scale, cam.Bottom() - bounds.Min[1]*scale}

	switch d.location {
	case TopLeftCorner:
		d.transform.SetPositionXY(tl[0], tl[1])
	case TopRightCorner:
		d.transform.SetPositionXY(br[0], tl[1])
	case BottomLeftCorner:
		d.transform.SetPositionXY(tl[0], br[1])
	case BottomRightCorner:
		d.transform.SetPositionXY(br[0], br[1])
	}
	d.transform.SetPositionZ(cam.Near() - 0.001)
}

func (d *DebugPanel) Destroy() {
	d.text.Destroy()
}

func (d *DebugPanel) Draw(cam *nora.OrthoCamera, renderState *nora.RenderState) {
	if !d.display {
		return
	}
	if d.textDirty {
		d.updateText()
		d.textDirty = false
	}
	d.updatePosition(cam)

	renderState.TransformStack.PushMulRight(d.transform.GetTransform())
	d.text.Draw(renderState)
	renderState.TransformStack.Pop()
}
