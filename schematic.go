package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/debug"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
	"github.com/sirupsen/logrus"
)

// Component represents an object that is placeable on the schematic.
type Component interface {
	// LocalBounds returns the component's bounding box in model-space.
	LocalBounds() vmath.Rectf
	// GlobalBounds returns the component's bounding box on the schematic.
	GlobalBounds() vmath.Rectf

	SetPosition(pos vmath.Vec2i)

	// Pins returns all component pins where wires can be connected in model-space.
	Pins() []*actor.Pin

	// Position returns the component's (global) position on the schematic.
	Position() vmath.Vec2i

	// Update is called each render frame.
	Update(elapsed time.Duration)

	// Draw draws the component
	Draw(renderState *nora.RenderState)

	Destroy()

	String() string
}

// Schematic contains all components and their positions.
type Schematic struct {
	cam            *nora.OrthoCamera
	interactionSys *nora.InteractionSystem
	gamePanel      *GamePanel
	domain         Domain

	gridSpace vmath.Mat4f // transforms components placed on the schematic into worldspace

	bounds vmath.Rectf

	outline    SchematicOutline
	components []Component
	Networking *Networking

	MakerOverlay *actor.MarkerOverlay
}

func NewSchematic(domain Domain, engine *nora.Engine, gamePanel *GamePanel, cellCount vmath.Vec2i) *Schematic {
	s := &Schematic{
		cam:            engine.Camera.(*nora.OrthoCamera),
		interactionSys: &engine.InteractionSystem,
		gamePanel:      gamePanel,
		domain:         domain,

		outline:    *NewSchematicOutline(cellCount),
		components: make([]Component, 0),

		MakerOverlay: actor.NewMarkerOverlay(),
	}
	s.bounds = vmath.RectfFromCorners(
		vmath.Vec2f{0, 0},
		cellCount.Vec2f(),
	)

	origin := s.outline.WorldCoord(vmath.Vec2i{0, 0})
	s.gridSpace = vmath.Mat4fFromTranslation(origin.Vec3f(0)).
		SetScaling(vmath.Vec3f{cellSize, cellSize, 1})

	s.Networking = NewNetworking(&engine.InteractionSystem, s, domain)

	assert.True(debug.MarkerOverlay == nil, "marker overlay should not be initialized yet (multiple schematics?)")
	debug.MarkerOverlay = s.MakerOverlay
	return s
}

// Clear resets the whole schematic by removing all components and networks.
func (s *Schematic) Clear() {
	for _, c := range s.components {
		c.Destroy()
	}
	s.components = make([]Component, 0)
	s.Networking.RemoveAllPins()
	s.Networking.Clear()
}

// Resize changes the size of the schematic
func (s *Schematic) Resize(cellCount vmath.Vec2i) bool {
	// TODO: check if all objects are still inside

	s.outline.Resize(cellCount)

	s.bounds = vmath.RectfFromCorners(
		vmath.Vec2f{0, 0},
		cellCount.Vec2f(),
	)

	origin := s.outline.WorldCoord(vmath.Vec2i{0, 0})
	newGridSpace := vmath.Mat4fFromTranslation(origin.Vec3f(0)).
		SetScaling(vmath.Vec3f{cellSize, cellSize, 1})
	assert.True(s.gridSpace.Equal(newGridSpace), "cannot change gridspace transform. It was already copied away")
	return true
}

func (s *Schematic) AddComponent(comp Component) error {
	if !s.bounds.ContainsRectf(comp.GlobalBounds()) {
		return fmt.Errorf("place component: outside schematic bounds")
	}

	conflicts := s.GetComponents(comp.GlobalBounds(), false)
	if len(conflicts) > 0 {

		logrus.Debugf("New component bounds: %v", comp.GlobalBounds())
		for _, c := range conflicts {
			logrus.Debugf("Conflicting bounds: %v", c.GlobalBounds())
		}
		return fmt.Errorf("place component: %d components are in the way", len(conflicts))
	}
	s.components = append(s.components, comp)
	s.Networking.AddPins(comp.Pins())
	return nil
}

// GetAllComponents returns all placed components.
func (s *Schematic) GetAllComponents() []Component {
	return s.components
}

// GetAllComponents returns all placed components.
func (s *Schematic) ComponentCount() int {
	return len(s.components)
}

// GetComponents returns all components in the given area.
// If mustCover is true, components are only returned if they are completely within the rectangle.
func (s *Schematic) GetComponents(area vmath.Rectf, mustCover bool) []Component {
	var comps []Component

	for _, c := range s.components {
		bounds := c.GlobalBounds()

		var found bool
		if mustCover {
			found = area.ContainsRectf(bounds)
		} else {
			found = area.Intersects(bounds)
		}
		if found {
			comps = append(comps, c)
		}
	}
	return comps
}

// GetComponent returns the component at the given position or nil if there is no component
func (s *Schematic) GetComponent(pos vmath.Vec2f) Component {
	for _, c := range s.components {
		bounds := c.GlobalBounds()
		if bounds.ContainsPoint(pos) {
			return c
		}
	}
	return nil
}

func (s *Schematic) Destroy() {
	s.cleanGamePanel()

	assert.True(debug.MarkerOverlay != nil, "should have been initialized")
	debug.MarkerOverlay = nil

	s.MakerOverlay.Destroy()
	s.Networking.Destroy()
	for _, c := range s.components {
		c.Destroy()
	}
	s.outline.Destroy()
	s.components = nil
}

// GridSize returns the number of positions on the schematic where things can be placed.
// These are the corners between cells.
func (s *Schematic) GridSize() vmath.Vec2i {
	return s.outline.GridSize()
}

// WorldCoord returns the position (in worldspace) of a specific snap point
func (s *Schematic) WorldCoord(cell vmath.Vec2i) vmath.Vec2f {
	return s.outline.WorldCoord(cell)
}

// SchemaCoord converts worldspace coordinates into schema coordinates.
func (s *Schematic) SchemaCoord(worldPos vmath.Vec2f) (vmath.Vec2f, bool) {
	return s.outline.SchemaCoord(worldPos)
}

// MouseSchemaCoord  converts the mouse position in schema coordinates.
func (s *Schematic) MouseSchemaCoord() (vmath.Vec2f, bool) {
	clip := s.interactionSys.MousePosClipSpace()
	world := s.cam.ClipSpaceToWorldSpace(clip)
	return s.SchemaCoord(world)
}

// SchemaPos returns the schema snap position nearest to the given world coordinate.
// Returns false if this snap position is outside the schematic.
func (s *Schematic) SchemaPos(worldPos vmath.Vec2f) (vmath.Vec2i, bool) {
	return s.outline.SchemaPos(worldPos)
}

// MouseSchemaPos returns the snap position nearest to the current mouse position.
// Returns false if this snap position is outside the schematic.
func (s *Schematic) MouseSchemaPos() (vmath.Vec2i, bool) {
	clip := s.interactionSys.MousePosClipSpace()
	world := s.cam.ClipSpaceToWorldSpace(clip)
	return s.SchemaPos(world)
}

func (s *Schematic) updateGamePanel() {
	mouseCoord, inside := s.MouseSchemaCoord()
	mousePos, _ := s.MouseSchemaPos()
	insideSym := " "
	if !inside {
		insideSym = "X"
	}

	networkName := "-"
	networkPins := "-"
	networkValue := "-"
	networks := s.Networking.GetNetworks(mousePos)
	if len(networks) > 0 {
		net := networks[0]
		networkName = fmt.Sprintf("%s (%d)", net.Name(), net.SegmentCount())
		if net.OutputPin() != nil {
			networkPins = "out + " + strconv.Itoa(len(net.InputPins())) + "x in"
		} else {
			networkPins = strconv.Itoa(len(net.InputPins())) + "x in"
		}
		value := s.domain.NetValue(net.NetID())
		networkValue = value.String(net.BitWidth(), logic.HEXDisplay)
	}

	componentName := "-"
	if comp := s.GetComponent(mouseCoord); comp != nil {
		componentName = comp.String()
	}

	s.gamePanel.Section("mouse position").
		Set("cell pos", mousePos.Format("[%7d x %7d]"+insideSym)).
		Set("cell coord", mouseCoord.Format("[%7.2f x %7.2f]")).
		Set("component", componentName).
		Set("network", networkName).
		Set("   pins", networkPins).
		Set("   value", networkValue)

	s.gamePanel.Section("schematic").
		Setf("components", strconv.Itoa(len(s.components))).
		Setf("networks", strconv.Itoa(s.Networking.NetworkCount()))
}

func (s *Schematic) cleanGamePanel() {
	s.gamePanel.RemoveSection("mouse position")
	s.gamePanel.RemoveSection("schematic")
}

// GridSpaceTransform returns the transformation matrix to draw content in grid space
func (s *Schematic) GridSpaceTransform() vmath.Mat4f {
	return s.gridSpace
}

func (s *Schematic) Update(elapsed time.Duration) {
	for _, comp := range s.components {
		comp.Update(elapsed)
	}

	s.updateGamePanel()
}

func (s *Schematic) Draw(renderState *nora.RenderState) {
	s.outline.Draw(renderState)

	renderState.TransformStack.Push()
	renderState.TransformStack.MulRight(s.gridSpace)

	for _, c := range s.components {
		c.Draw(renderState)
	}
	s.Networking.Draw(renderState)

	// move marker overlay towards camera:
	renderState.TransformStack.MulRight(vmath.Mat4fFromTranslation(vmath.Vec3f{0, 0, 0.8}))
	s.MakerOverlay.Draw(renderState)

	renderState.TransformStack.Pop()
}
