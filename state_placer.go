package main

import (
	"fmt"

	"github.com/maja42/glfw"
	"github.com/maja42/logicat/action"
	"github.com/maja42/logicat/actor/gates"
	"github.com/maja42/logicat/logic"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
	"github.com/sirupsen/logrus"
)

type ComponentType int

const (
	AndGate = ComponentType(iota)
	OrGate
	NotGate
	XorGate
	Button
	Lamp
	ComponentCount // for counting the number of components
)

type Placer struct {
	cam            *nora.OrthoCamera
	interactionSys *nora.InteractionSystem
	mpanel         *action.Panel
	schematic      *Schematic
	gridSpace      vmath.Mat4f

	netQuery logic.NetValueQueryFunc

	leftMouseCBID nora.CallbackID
	keyPressCBID  nora.CallbackID

	componentType ComponentType
	pinCount      int
	component     Component
}

func NewPlacer(interactionSys *nora.InteractionSystem, mpanel *action.Panel, schematic *Schematic, netQuery logic.NetValueQueryFunc) *Placer {
	a := &Placer{
		interactionSys: interactionSys,
		mpanel:         mpanel,
		schematic:      schematic,
		gridSpace:      schematic.GridSpaceTransform(),

		netQuery: netQuery,

		pinCount: 4,
	}

	a.component = gates.NewAndGate(a.pinCount)

	a.leftMouseCBID = a.interactionSys.OnMouseButton(glfw.MouseButtonLeft, glfw.Release, a.onLeftMouseButton)
	a.keyPressCBID = a.interactionSys.OnKeyEvent(a.onKeyEvent)
	// TODO:
	// 	[Space] 		Rotate component clock-wise
	//  [shift-space]	Rotate component counter clock-wise
	//  [arrows]  	    Set component rotation west/north/east/south
	return a
}

func (a *Placer) Destroy() {
	a.interactionSys.RemoveMouseButtonEventFunc(a.leftMouseCBID)
	a.interactionSys.RemoveKeyEventFunc(a.keyPressCBID)
	a.component.Destroy()
}

func (a *Placer) createComponent(typ ComponentType) Component {
	switch typ {
	case AndGate:
		return gates.NewAndGate(a.pinCount)
	case OrGate:
		return gates.NewOrGate(a.pinCount)
	case NotGate:
		return gates.NewNotGate(a.pinCount)
	case XorGate:
		return gates.NewXorGate(a.pinCount)
	case Button:
		return gates.NewButton(a.mpanel, a.pinCount)
	case Lamp:
		return gates.NewLamp(a.pinCount, logic.HEXDisplay, a.netQuery)
	}
	assert.Fail("unknown component type")
	return nil
}

func (a *Placer) onLeftMouseButton(glfw.ModifierKey) {
	if err := a.schematic.AddComponent(a.component); err != nil {
		logrus.Warnf("Cannot place component: %s", err)
	} else {
		a.component = a.createComponent(a.componentType)
	}
}

func (a *Placer) scrollPinCount(increase bool) {
	if increase && a.pinCount < 32 {
		a.pinCount++
	} else if !increase && a.pinCount > 1 {
		a.pinCount--
	} else {
		return
	}
	a.component.Destroy()
	a.component = a.createComponent(a.componentType)
}

func (a *Placer) scrollComponent(next bool) {
	if next {
		a.setComponent((a.componentType + 1) % ComponentCount)
	} else {
		a.setComponent((a.componentType + ComponentCount - 1) % ComponentCount)
	}
}

func (a *Placer) setComponent(compType ComponentType) {
	a.componentType = compType
	a.component.Destroy()
	a.component = a.createComponent(a.componentType)
}

func (a *Placer) onKeyEvent(key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	if (key == glfw.KeyPageUp || key == glfw.KeyPageDown) && (action == glfw.Press || action == glfw.Repeat) {
		a.scrollComponent(key == glfw.KeyPageUp)
		return
	}
	if (key == glfw.KeyKPAdd || key == glfw.KeyKPSubtract) && (action == glfw.Press || action == glfw.Repeat) {
		a.scrollPinCount(key == glfw.KeyKPAdd)
		return
	}

	if key >= glfw.Key1 && key <= glfw.Key9 && action == glfw.Press {
		compType := ComponentType(key - glfw.Key1)
		if compType >= 0 && compType < ComponentCount {
			a.setComponent(compType)
		}
		return
	}
}

func (a *Placer) Update() {
	snap, ok := a.schematic.MouseSchemaPos()
	_ = ok

	if a.component.Position() == snap {
		return // position did not change
	}
	a.component.SetPosition(snap)
}

func (a *Placer) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.PushMulRight(a.gridSpace)
	a.component.Draw(renderState)
	renderState.TransformStack.Pop()
}

func (a *Placer) String() string {
	return fmt.Sprintf("place comp (%v)", a.component.String())
}
