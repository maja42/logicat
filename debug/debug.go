package debug

import "github.com/maja42/logicat/actor"

// Can be used to place debug markers on the schematic
var MarkerOverlay *actor.MarkerOverlay
