package main

import (
	"context"
	"math/rand"
	"time"

	"github.com/maja42/gl"
	"github.com/maja42/glfw"
	"github.com/maja42/vmath"

	"github.com/maja42/nora"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/color"
	"github.com/sirupsen/logrus"
)

const noraBuiltin = "../nora/builtin"

/*
	Rough edges:
		DONE  -  Draw-Order (Ordering of models)
		DONE  - Ordering of jobs
		DONE  - Jobs- und Render-Loop
		DONE 	==> move outside of Nora? Remove job-system?
		DONE - shader hot-reloading broke
		DONE - calling gl functions within key-callbacks deadlocks (=>crashes) the application
		DONE - when doing gl-calls asynchronously (eg. deleting + drawing buffers), there is a chance to get an exception within the C code:
		DONE 			Exception 0xc0000005 0x0 0x8 0x51ff9550
		DONE 			PC=0x51ff9550
		DONE 			signal arrived during external code execution
  		DONE   The exit code is also 0xC0000005.
		DONE   If the application crashes due to this issue, the above error is not always printed (but the exit-code is available).
		DONE   It was reproduced when a go-routine started within a keyboard-callback-function deleted a mesh, and replaced it with a new one.
		DONE   This issue is likely not completely fixable: If the user starts go-routines to do work (which is supported),
		DONE 	but forgets to synchronize access (which is only done on a gl-command level, not on a mesh level), this issue can happen.
		DONE 	Document it in the engine.
		- Multi-Window & Context support
			This also fixes the global variables in nora ("engine") which are not nice
*/

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})
	logrus.SetLevel(logrus.DebugLevel)
	logrus.Info("Started")

	rand.Seed(time.Now().UnixNano())

	err := run()
	if err != nil {
		logrus.Fatalln(err)
	}
}

func run() error {
	if err := nora.Init(); err != nil {
		return err
	}
	defer nora.Destroy()

	windowSize := vmath.Vec2i{1920 * 1.5, 1080 * 1.5}
	engine, err := nora.CreateWindow(nora.Settings{
		WindowTitle:  "Logicat",
		WindowSize:   windowSize,
		ResizePolicy: nora.ResizeKeepAspectRatio,
		Samples:      4,
	})
	if err != nil {
		return err
	}
	defer engine.Destroy()

	engine.Window().SetPos(3840*1.5-windowSize[0]/2.0, 2160/2-windowSize[1]/2)

	if err := engine.Shaders.LoadAll(shader.Builtins(noraBuiltin + "/shader")); err != nil {
		logrus.Errorf("Failed to load builtin shaders: %s", err)
	}

	go engine.Shaders.StartHotReloading(context.Background())
	go engine.Textures.StartHotReloading(context.Background())

	engine.SetClearColor(color.Gray(0.1))

	game := NewGame(engine)
	defer game.Destroy()

	cam := engine.Camera.(*nora.OrthoCamera)
	cam.SetOrthoWidth(100)
	cam.MoveXY(-10, 0)

	font, err := nora.LoadFont(noraBuiltin + "/fonts/ibm plex mono/ibm_plex_mono_regular_32.xml")
	if err != nil {
		logrus.Errorf("Failed to load font: %s", err)
	}
	_ = font

	enginePanel := NewEnginePanel(engine, font)

	enableWireframeSwitching(engine)

	stop := false
	engine.InteractionSystem.OnKey(glfw.KeyEscape, glfw.Press, func(_ glfw.ModifierKey) {
		logrus.Info("ESC: exiting")
		stop = true
	})

	engine.Render(func(elapsed time.Duration, renderState *nora.RenderState) bool {
		game.Frame(elapsed, renderState)
		enginePanel.Update()
		enginePanel.Draw(renderState)
		return stop
	})
	return nil
}

func enableWireframeSwitching(engine *nora.Engine) {
	var mode gl.Enum = gl.FILL
	engine.InteractionSystem.OnKey(glfw.KeyTab, glfw.Press, func(key glfw.ModifierKey) {
		if mode == gl.FILL {
			mode = gl.LINE
		} else {
			mode = gl.FILL
		}
		gl.PolygonMode(mode)
	})
}

//func analyze(engine *nora.Engine, font *nora.Font) (fontBrightness, error) {
//	fontTexPath := engine.Textures.Definition(font.TextureKey()).Path
//
//	imgFile, err := os.Open(fontTexPath)
//	if err != nil {
//		return fontBrightness{}, fmt.Errorf("open texture file %q: %v", fontTexPath, err)
//	}
//	img, _, err := image.Decode(imgFile)
//	imgFile.Close()
//	if err != nil {
//		return fontBrightness{}, fmt.Errorf("decode texture file %q: %v", fontTexPath, err)
//	}
//	imgSize := img.Bounds().Size()
//
//	//brightness := make(map[rune]float32)
//	fb := make(fontBrightness, 0, len(font.Chars))
//	maxBrightness := float32(0)
//
//	//chrs := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
//	chrs := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ÖÄÜöäü!()=?.-;:_+*~#'"
//
//	for r, c := range font.Chars {
//		if strings.IndexRune(chrs, r) < 0 {
//			continue
//		}
//
//		var b float32
//		for y := c.Pos[1]; y < c.Pos[1]+c.Size[1]; y++ {
//			for x := c.Pos[0]; x < c.Pos[0]+c.Size[0]; x++ {
//				assert.True(x >= 0 && x < imgSize.X, "tex pos x")
//				assert.True(y >= 0 && y < imgSize.Y, "tex pos y")
//				_, _, _, a := img.At(x, y).RGBA()
//				b += float32(a) / 0xffff
//			}
//		}
//		if b > maxBrightness {
//			maxBrightness = b
//		}
//		fb = append(fb, runeBrightness{
//			r:          r,
//			brightness: b,
//		})
//	}
//	for i := range fb { // normalize
//		fb[i].brightness /= maxBrightness
//	}
//	sort.Sort(fb)
//	return fb, nil
//}
//
//type fontBrightness []runeBrightness
//
//func (fb fontBrightness) Nearest(brightness float32) rune {
//	if len(fb) == 0 {
//		var r rune
//		return r
//	}
//
//	// todo: binary search
//	for i, c := range fb {
//		if c.brightness >= brightness {
//			prevC := c
//			if i > 0 {
//				prevC = fb[i-1]
//			}
//
//			diff := absFloat(c.brightness - brightness)
//			prevDiff := absFloat(prevC.brightness - brightness)
//			if diff < prevDiff {
//				return c.r
//			}
//			return prevC.r
//		}
//	}
//	return fb[len(fb)-1].r
//}
//
//func absFloat(f float32) float32 {
//	if f < 0 {
//		return -f
//	}
//	return f
//}
//
//type runeBrightness struct {
//	r          rune
//	brightness float32
//}
//
//func (fb fontBrightness) Len() int {
//	return len(fb)
//}
//
//func (fb fontBrightness) Less(i, j int) bool {
//	return fb[i].brightness < fb[j].brightness
//}
//
//func (fb fontBrightness) Swap(i, j int) {
//	fb[i], fb[j] = fb[j], fb[i]
//}
