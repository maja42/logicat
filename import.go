package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/vmath"
)

func ImportSchematicFromFile(domain Domain, engine *nora.Engine, gamePanel *GamePanel, srcPath string) (*Schematic, error) {
	file, err := os.Open(srcPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return ImportSchematic(domain, engine, gamePanel, file)
}

func ImportSchematic(domain Domain, engine *nora.Engine, gamePanel *GamePanel, in io.Reader) (*Schematic, error) {
	data, err := ioutil.ReadAll(in)
	if err != nil {
		return nil, err
	}
	var export schematicExport
	if err := json.Unmarshal(data, &export); err != nil {
		return nil, fmt.Errorf("unmarshal data: %w", err)
	}

	schematic := NewSchematic(domain, engine, gamePanel, export.Size)

	if err := schematic.Networking.Import(export.Networks); err != nil {
		schematic.Destroy()
		return nil, fmt.Errorf("import networks: %w", err)
	}

	return schematic, nil
}

func (a *Networking) Import(networks []networkExport) error {
	for _, net := range networks {
		network := actor.NewNetwork(a.domain, a.wirePool, a.bondPool)
		network.SetName(net.Name)

		for _, seg := range net.Segments {
			network.AddWireSegment(utils.LineSegment2i{
				Start: vmath.Vec2i{seg[0], seg[1]},
				End:   vmath.Vec2i{seg[2], seg[3]},
			})
		}

		a.networks = append(a.networks, network)
	}
	return nil
}
