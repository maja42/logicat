package main

import (
	"github.com/maja42/glfw"
	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
)

// Selector is used to place new wires.
type Selector struct {
	interactionSys *nora.InteractionSystem
	schematic      *Schematic
	networking     *Networking
	gridSpace      vmath.Mat4f

	mouseEventCBID nora.CallbackID
	rightMouseCBID nora.CallbackID

	// segment dragging:
	network      *actor.Network
	segment      utils.LineSegment2i
	neighbours   []utils.LineSegment2i
	segmentMover *SegmentMover
}

func NewSelector(interactionSys *nora.InteractionSystem, schematic *Schematic, networking *Networking) *Selector {
	a := &Selector{
		interactionSys: interactionSys,
		schematic:      schematic,
		networking:     networking,
		gridSpace:      schematic.GridSpaceTransform(),

		segmentMover: NewSegmentMovement(schematic),
	}

	a.mouseEventCBID = a.interactionSys.OnMouseButtonEvent(a.mouseEvent)
	a.rightMouseCBID = a.interactionSys.OnMouseButton(glfw.MouseButtonRight, glfw.Release, a.rightButton)
	return a
}

func (a *Selector) mouseEvent(button glfw.MouseButton, action glfw.Action, _ glfw.ModifierKey) {
	if button != glfw.MouseButtonLeft {
		return
	}

	mouse, inside := a.schematic.MouseSchemaPos()
	if !inside {
		return
	}

	if action == glfw.Press {
		info := a.networking.StealSegment(mouse)
		if info == nil {
			return
		}
		a.segmentMover.Configure(info)
	} else {
		assert.True(action == glfw.Release, "expected release action")
		a.segmentMover.Release()
	}
}

func (a *Selector) rightButton(glfw.ModifierKey) {
	a.segmentMover.Abort()
}

func (a *Selector) Destroy() {
	a.interactionSys.RemoveMouseButtonEventFunc(a.mouseEventCBID)
	a.interactionSys.RemoveMouseButtonEventFunc(a.rightMouseCBID)

	a.segmentMover.Destroy()
}

func (a *Selector) Update() {
	mouse, inside := a.schematic.MouseSchemaPos()
	if !inside {
		//return
	}
	a.segmentMover.Update(mouse)
}

func (a *Selector) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.PushMulRight(a.gridSpace)
	a.segmentMover.Draw(renderState)
	renderState.TransformStack.Pop()
}

func (a *Selector) String() string {
	return "selecting"
}
