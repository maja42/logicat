package main

import (
	"fmt"

	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
)

type NetworkingConflictType int

const (
	NoConflict               = NetworkingConflictType(iota)
	OutsideSchematicConflict // Wire is outside the schematic
	NetworkConflict          // Multiple incompatible networks are merged; This connects two ore more output pins with each other
	LoopConflict             // A wire or network has a loop
)

var conflictStrings = map[NetworkingConflictType]string{
	NoConflict:               "no conflict",
	OutsideSchematicConflict: "outside schema",
	NetworkConflict:          "network",
	LoopConflict:             "loop",
}

func (c NetworkingConflictType) String() string {
	str, ok := conflictStrings[c]
	if ok {
		return str
	}
	return fmt.Sprintf("unknown conflict (%d)", c)
}

type Bonds struct {
	Points   map[vmath.Vec2i]NetworkingConflictType // All new connection points that will form (does not contain pin connections)
	Networks map[*actor.Network]vmath.Vec2i         // All new networks that will be joined (and where they will be joined)

	OutputPin *actor.Pin    // The output pin that will drive the network (if any)
	InputPins actor.PinList // All new input pins that will be joined
}

func (b *Bonds) Positions() ([]vmath.Vec2i, []vmath.Vec2i) {
	good := make([]vmath.Vec2i, 0, len(b.Points))
	bad := make([]vmath.Vec2i, 0, len(b.Points))
	for pos, conflict := range b.Points {
		if conflict == NoConflict {
			good = append(good, pos)
		} else {
			bad = append(bad, pos)
		}
	}
	return good, bad
}

func (b *Bonds) HasConflict() bool {
	for _, c := range b.Points {
		if c != NoConflict {
			return true
		}
	}
	return false
}

// ApplyVia defines the via's network that was unknown during bond calculation.
// The via is the network of the previous segment, that was still unknown during bonding.
// The mergeList are all networks that were merged by the via. They do no longer exist.
// It is not allowed to add any other network via this function.
func (b *Bonds) ApplyVia(pos vmath.Vec2i, via *actor.Network, mergeList map[*actor.Network]vmath.Vec2i) {
	assert.False(b.HasConflict(), "expected bonds to have no conflict")

	// Placing the previous segment merged all its networks.
	// If we are referencing any of those: remove them, as they got replaced and no longer exist.
	for net := range mergeList {
		delete(b.Networks, net)
	}

	// Replace with merged (or newly created) network:
	b.Networks[via] = pos
	b.Points[pos] = NoConflict
}

type bonder struct {
	networking *Networking
	segment    utils.LineSegment2i // Segment for which bonds to calculate
	points     []vmath.Vec2i       // All points on the segment
	via        *Bonds              // Optional: bonds of a first preview-segment that is connected

	bonds *Bonds
}

func CalculateBonds(networking *Networking, via *Bonds, segment utils.LineSegment2i) *Bonds {
	// Finding connection points:
	//		- Check if there are pins on the current line segments
	//		- Check if other networks have "corners" on positions of the current line segments
	//			Corners are:	- loose ends
	//							- wire turns (direction change)
	//							- wires touching a pin (on a loose end, wire turn or straight trough)
	//							= all these "corners" are represented as segment-endpoints in a network
	//		- Check if we create new "corners" that would touch other wires
	// Finding conflicts:
	// 		- Conflicting connection points
	//		- Overlapping line segments
	//		- Wire loops with itself
	//		- Wire creates a loop with another network (two connection points with the same network)

	bonds := &Bonds{
		Points:    make(map[vmath.Vec2i]NetworkingConflictType),
		Networks:  make(map[*actor.Network]vmath.Vec2i),
		OutputPin: nil,
		InputPins: make(actor.PinList),
	}
	bonder := &bonder{
		networking: networking,
		segment:    segment,
		points:     utils.PointsOnSegment(segment),
		via:        via,
		bonds:      bonds,
	}

	bonder.findPins()
	bonder.findTouchingNetworks()
	bonder.findTouchedNetworks()
	return bonds
}

func (b *bonder) findPins() {
	// Get all pins touching the new segments
	touchedPins := b.networking.GetPins(b.points)

	var out *actor.Pin // The output pin that will drive the network (if any)
	var in = make(actor.PinList)
	var points = make(map[vmath.Vec2i]NetworkingConflictType)

	if b.via != nil {
		out = b.via.OutputPin
	}

	for _, pin := range touchedPins {
		conflict := NoConflict
		if pin.Type() == actor.OutputPin {
			if out != nil && out != pin {
				conflict = NetworkConflict
			}
			out = pin
		} else {
			in[pin] = struct{}{}
		}
		points[pin.SchemaPos()] = conflict
	}
	b.bonds.Points = points
	b.bonds.OutputPin = out
	b.bonds.InputPins = in
}

func (b *bonder) findTouchingNetworks() {
	// Get all other networks touching the new segment
	for _, net := range b.networking.Networks() {
		endpoints := net.SegmentEndpoints()
		for pos := range endpoints {
			if b.segment.Contains(pos) {
				b.addNetwork(pos, net)
			}
		}
	}
}

func (b *bonder) findTouchedNetworks() {
	// Get networks we are touching
	corners := []vmath.Vec2i{b.segment.Start, b.segment.End}
	for _, corner := range corners {
		if _, ok := b.bonds.Points[corner]; ok { // already identified
			continue
		}
		networks := b.networking.GetNetworks(corner)
		for _, net := range networks {
			b.addNetwork(corner, net)
		}
	}
}

func (b *bonder) addPoint(pos vmath.Vec2i, conflict NetworkingConflictType) bool {
	existingConflict, ok := b.bonds.Points[pos]
	if existingConflict == NoConflict || !ok {
		b.bonds.Points[pos] = conflict
	}
	return !ok
}

func (b *bonder) addNetwork(pos vmath.Vec2i, net *actor.Network) {
	conflict := NoConflict

	netOut := net.OutputPin()
	if netOut != nil && netOut != b.bonds.OutputPin {
		if b.bonds.OutputPin != nil {
			conflict = NetworkConflict
		}
		b.bonds.OutputPin = netOut
	}

	// Note:
	//		If I want to forbid loops (again), the following code works, but also forbids overlaps.
	//		To fix it, allow loops if both bonds are on the same segment. And check for other special cases!
	//		Also add an assertion to the "startWire == endWire" in network.go!
	//if otherPos, ok := b.bonds.Networks[net]; ok && otherPos != pows {
	//	// There is a loop conflict. Mark the previous connection-point (on this segment) as a conflict too
	//	b.bonds.Points[otherPos] = LoopConflict
	//	conflict = LoopConflict
	//} else if b.via != nil {
	//	if otherPos, ok := b.via.Networks[net]; ok && otherPos != pos {
	//		conflict = LoopConflict
	//	}
	//}
	b.addPoint(pos, conflict)
	b.bonds.Networks[net] = pos
}
