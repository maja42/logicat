package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/maja42/vmath"
)

type schematicExport struct {
	Size     vmath.Vec2i
	Networks []networkExport
}

type networkExport struct {
	Name     string `json:",omitempty"`
	Segments []segmentExport
}

type segmentExport [4]int

func (s *Schematic) ExportToFile(destPath string) error {
	file, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer file.Close()
	return s.Export(file)
}

func (s *Schematic) Export(out io.Writer) error {
	export := schematicExport{
		Size:     s.bounds.Size().Vec2i(),
		Networks: s.Networking.Export(),
	}
	data, err := json.Marshal(export)
	if err != nil {
		return fmt.Errorf("marshal exported data: %w", err)
	}
	if _, err := out.Write(data); err != nil {
		return fmt.Errorf("write exported data: %w", err)
	}
	return nil
}

func (a *Networking) Export() []networkExport {
	export := make([]networkExport, len(a.networks))
	for idx, net := range a.networks {
		expSegments := make([]segmentExport, net.SegmentCount())
		segments := net.Segments()
		for segIdx, seg := range segments {
			expSegments[segIdx] = [4]int{
				seg.Start[0],
				seg.Start[1],
				seg.End[0],
				seg.End[1],
			}
		}

		export[idx] = networkExport{
			Name:     net.RealName(),
			Segments: expSegments,
		}
	}
	return export
}
