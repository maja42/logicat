package main

import (
	"fmt"
	"strconv"

	"github.com/maja42/glfw"
	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/theme"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/vmath"
	"github.com/sirupsen/logrus"
)

type connectPriority int

const (
	connectStraightFirst = connectPriority(iota)
	connectDiagonalFirst
)

type WirePreview struct {
	Segment utils.LineSegment2i
	Wire    *actor.Wire
	Bonds   *Bonds
}

// WireTool is used to place new wires.
type WireTool struct {
	interactionSys *nora.InteractionSystem
	schematic      *Schematic
	networking     *Networking
	gridSpace      vmath.Mat4f

	gamePanel *GamePanel

	leftMouseCBID  nora.CallbackID
	rightMouseCBID nora.CallbackID
	spaceKeyCBID   nora.CallbackID

	lastMouseSchemaPos vmath.Vec2i
	forceUpdate        bool // if the new wire segment must be updated even if the mouse position did not change

	connectPriority connectPriority

	previewSize   int
	startingPoint vmath.Vec2i
	preview       [2]WirePreview
}

func NewWireTool(interactionSys *nora.InteractionSystem, schematic *Schematic, networking *Networking, gamePanel *GamePanel) *WireTool {
	a := &WireTool{
		interactionSys: interactionSys,
		schematic:      schematic,
		networking:     networking,
		gridSpace:      schematic.GridSpaceTransform(),
		gamePanel:      gamePanel,

		forceUpdate: true,
	}

	a.startingPoint = vmath.Vec2i{-1, -1}
	for i := 0; i < len(a.preview); i++ {
		a.preview[i].Wire = actor.NewWire()
		a.preview[i].Wire.SetColor(theme.WirePlacementColor)
	}

	a.leftMouseCBID = a.interactionSys.OnMouseButton(glfw.MouseButtonLeft, glfw.Release, a.leftButton)
	a.rightMouseCBID = a.interactionSys.OnMouseButton(glfw.MouseButtonRight, glfw.Release, a.rightButton)
	a.spaceKeyCBID = a.interactionSys.OnKey(glfw.KeySpace, glfw.Release, a.space)
	return a
}

func (a *WireTool) hasStartingPoint() bool {
	return a.startingPoint[0] >= 0
}

func (a *WireTool) leftButton(glfw.ModifierKey) {
	if !a.hasStartingPoint() { //define starting point
		mouse, inside := a.schematic.MouseSchemaPos()
		if inside {
			if a.networking.CanConnect(mouse) {
				a.networking.TryBond(mouse)
				a.startingPoint = mouse
			}
		}
		return
	}
	a.Submit()
}

func (a *WireTool) rightButton(glfw.ModifierKey) {
	//a.Submit()
	a.startingPoint = vmath.Vec2i{-1, -1}
	a.forceUpdate = true
}

func (a *WireTool) space(glfw.ModifierKey) {
	if a.connectPriority == connectStraightFirst {
		a.connectPriority = connectDiagonalFirst
	} else {
		a.connectPriority = connectStraightFirst
	}
	a.forceUpdate = true
}

func (a *WireTool) Submit() {
	var net *actor.Network
	var mergeList map[*actor.Network]vmath.Vec2i

	for i := 0; i < a.previewSize; i++ {
		preview := &a.preview[i]
		if preview.Bonds.HasConflict() {
			logrus.Warn("Cannot submit wire (conflict)")
			return
		}
		if net != nil {
			preview.Bonds.ApplyVia(preview.Segment.Start, net, mergeList)
		}
		net = a.networking.AddWireSegment(preview.Segment, preview.Bonds)
		mergeList = preview.Bonds.Networks // List of networks that were merged
		a.startingPoint = preview.Segment.End
	}
	a.previewSize = 0
	a.forceUpdate = true
}

func (a *WireTool) Reset() {
	a.previewSize = 0
	a.forceUpdate = true
}

func (a *WireTool) Destroy() {
	a.cleanGamePanel()

	a.schematic.MakerOverlay.RemoveBonds()

	for i := 0; i < len(a.preview); i++ {
		a.preview[i].Wire.Destroy()
	}

	a.interactionSys.RemoveMouseButtonEventFunc(a.leftMouseCBID)
	a.interactionSys.RemoveMouseButtonEventFunc(a.rightMouseCBID)
	a.interactionSys.RemoveKeyEventFunc(a.spaceKeyCBID)
}

func (a *WireTool) Update() {
	mouse, inside := a.schematic.MouseSchemaPos()
	//if !a.forceUpdate && mouse == a.lastMouseSchemaPos {
	//	return // no changes
	//}
	a.lastMouseSchemaPos = mouse
	defer a.updateGamePanel()

	a.previewSize = 0

	a.showBonds(nil, nil)
	if !a.hasStartingPoint() && inside {
		a.showHook(mouse, false)
		if pin := a.schematic.Networking.GetPin(mouse); pin != nil {
			a.showHook(mouse, true)
		} else if a.schematic.Networking.HasNetworks(mouse) {
			good := a.schematic.Networking.CanConnect(mouse)
			a.showHook(mouse, good)
		}
	}

	if !a.hasStartingPoint() { // no starting point yet; waiting for click
		return
	}

	// Update + show wire preview
	startingPoint := a.startingPoint

	segments := a.calculateWireSegments(startingPoint, mouse)

	a.previewSize = len(segments)

	var good, bad []vmath.Vec2i
	var previousSegBonds *Bonds
	for i, seg := range segments {
		preview := &a.preview[i]
		preview.Segment = seg
		preview.Bonds = CalculateBonds(a.networking, previousSegBonds, seg)
		preview.Wire.ClearPoints()
		preview.Wire.AddPoints(seg.Start, seg.End)

		g, b := preview.Bonds.Positions()
		good = append(good, g...)
		bad = append(bad, b...)
		previousSegBonds = preview.Bonds
	}
	a.showBonds(good, bad)
}

func (a *WireTool) showHook(pos vmath.Vec2i, good bool) {
	if good {
		a.schematic.MakerOverlay.ShowBonds([]vmath.Vec2i{pos}, nil)
	} else {
		a.schematic.MakerOverlay.ShowBonds(nil, []vmath.Vec2i{pos})
	}
}

func (a *WireTool) showBonds(good, bad []vmath.Vec2i) {
	a.schematic.MakerOverlay.ShowBonds(good, bad)
}

func (a *WireTool) updateGamePanel() {
	section := a.gamePanel.Section("wire tool").
		Clear().
		Setf("starting point", a.startingPoint.Format("[%4d x %4d]")).
		Setf("preview length", "%d", a.previewSize)

	for i := 0; i < a.previewSize; i++ {
		prefix := "[" + strconv.Itoa(i) + "]"
		bonds := a.preview[i].Bonds
		section.
			Setf(prefix+" output pin", "%t", bonds.OutputPin != nil).
			Setf(prefix+" input pins", "%d", len(bonds.InputPins)).
			Setf(prefix+" networks", "%d", len(bonds.Networks)).
			Setf(prefix+" any conflict", "%t", bonds.HasConflict())
	}
}

func (a *WireTool) cleanGamePanel() {
	a.gamePanel.RemoveSection("wire tool")
}

// calculateWireSegments returns the segments to create a wire from start to dest.
// Returns at most one straight and one diagonal segment.
// Guarantees that the returned segments don't have zero length.
func (a *WireTool) calculateWireSegments(start, dest vmath.Vec2i) []utils.LineSegment2i {
	vec := dest.Sub(start)
	absVec := vec.Abs()

	var straightVec vmath.Vec2i

	if absVec[0] > absVec[1] { // x-distance is bigger -> horizontal line
		x := absVec[0] - absVec[1]
		if vec[0] > 0 { // to the right
			straightVec[0] = x
		} else { // to the left
			straightVec[0] = -x
		}
	} else { // vertical line
		y := absVec[1] - absVec[0]
		if vec[1] > 0 { // upwards
			straightVec[1] = y
		} else { // downwards
			straightVec[1] = -y
		}
	}
	var middlePoint vmath.Vec2i
	switch a.connectPriority {
	case connectStraightFirst:
		middlePoint = start.Add(straightVec)
	case connectDiagonalFirst:
		diagonalVec := dest.Sub(start.Add(straightVec))
		middlePoint = start.Add(diagonalVec)
	}

	lastPoint := start
	segments := make([]utils.LineSegment2i, 0, 2)

	if middlePoint != start {
		segments = append(segments, utils.LineSegment2i{
			Start: lastPoint,
			End:   middlePoint,
		})
		lastPoint = middlePoint
	}
	if dest != middlePoint {
		segments = append(segments, utils.LineSegment2i{
			Start: lastPoint,
			End:   dest,
		})
		lastPoint = dest
	}
	return segments
}

func (a *WireTool) Draw(renderState *nora.RenderState) {
	renderState.TransformStack.PushMulRight(a.gridSpace)

	for i := 0; i < a.previewSize; i++ {
		a.preview[i].Wire.Draw(renderState)
	}

	renderState.TransformStack.Pop()
}

func (a *WireTool) String() string {
	return fmt.Sprintf("place wire")
}
