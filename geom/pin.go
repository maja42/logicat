package geom

import (
	"github.com/maja42/gl"
	"github.com/maja42/logicat/theme"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/geometry/geo2d"
	"github.com/maja42/vmath"
)

type Orientation int

const (
	Up = Orientation(iota)
	Down
	Left
	Right
)

func (o Orientation) Vec2f() vmath.Vec2f {
	switch o {
	case Up:
		return vmath.Vec2f{0, -1}
	case Down:
		return vmath.Vec2f{0, 1}
	case Left:
		return vmath.Vec2f{1, 0}
	case Right:
		return vmath.Vec2f{-1, 0}
	}
	assert.Fail("invalid orientation")
	return vmath.Vec2f{}
}

type PinMark int

const (
	NoMark     = PinMark(0x00)
	CircleMark = PinMark(0x01)
)

func Pin(pos vmath.Vec2i, orientation Orientation, marks PinMark, connected bool) *nora.Geometry {
	vec := orientation.Vec2f()

	fpos := pos.Vec2f()
	endPos := fpos.Add(vec.MulScalar(theme.PinLength))

	pinStartPos := fpos.Sub(vec.MulScalar(theme.PinWidth * 0.5)) // extend by half a pinWidth
	pinEndPos := endPos

	circleMark := marks&CircleMark != 0
	circleMarkPos := endPos.Sub(vec.MulScalar(theme.PinNotSymbolInnerRadius + theme.BodyBorderWidth/2))
	if circleMark { //do not penetrate inner radius
		pinEndPos = circleMarkPos.Sub(vec.MulScalar(theme.PinNotSymbolInnerRadius))
	}

	/*
	   2 --------- 3    +--->vec
	   |     \     |    |
	   0 --------- 1    v norm
	*/
	pinNorm := vec.NormalVec(false).MulScalar(theme.PinWidth * 0.5)
	p0 := pinStartPos.Add(pinNorm)
	p1 := pinEndPos.Add(pinNorm)
	p2 := pinStartPos.Sub(pinNorm)
	p3 := pinEndPos.Sub(pinNorm)

	vertices := []float32{
		p0[0], p0[1], p1[0], p1[1],
		p2[0], p2[1], p3[0], p3[1],
	}

	pin := nora.NewGeometry(4, vertices, nil, gl.TRIANGLE_STRIP, []string{"position"}, nora.InterleavedBuffer)
	if circleMark {
		pin.AppendGeometry(geo2d.Ring(circleMarkPos, theme.PinNotSymbolOuterRadius, theme.PinNotSymbolInnerRadius, 32))
	}
	if connected {
		pin.AppendGeometry(geo2d.Circle(fpos, theme.PinBondRadius, 32))
	}
	return pin
}
