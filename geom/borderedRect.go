package geom

import (
	"github.com/maja42/gl"
	"github.com/maja42/nora"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

// BorderedRect returns the geometry for a rectangle with a border
func BorderedRect(r vmath.Rectf, borderWidth float32, rectColor, borderColor color.Color) *nora.Geometry {
	bw2 := borderWidth / 2
	outerMin := vmath.Vec2f{r.Left() - bw2, r.Bottom() - bw2}
	outerMax := vmath.Vec2f{r.Right() + bw2, r.Top() + bw2}

	innerMin := vmath.Vec2f{r.Left() + bw2, r.Bottom() + bw2}
	innerMax := vmath.Vec2f{r.Right() - bw2, r.Top() - bw2}

	/* counter-clockwise
	   3 - 2
	   | / |
	   0 - 1
	*/

	vertices := []float32{
		outerMin[0], outerMin[1], borderColor.R, borderColor.G, borderColor.B,
		outerMax[0], outerMin[1], borderColor.R, borderColor.G, borderColor.B,
		outerMax[0], outerMax[1], borderColor.R, borderColor.G, borderColor.B,
		outerMin[0], outerMax[1], borderColor.R, borderColor.G, borderColor.B,

		innerMin[0], innerMin[1], rectColor.R, rectColor.G, rectColor.B,
		innerMax[0], innerMin[1], rectColor.R, rectColor.G, rectColor.B,
		innerMax[0], innerMax[1], rectColor.R, rectColor.G, rectColor.B,
		innerMin[0], innerMax[1], rectColor.R, rectColor.G, rectColor.B,
	}

	indices := []uint16{
		// outer
		0, 1, 2,
		0, 2, 3,
		// inner
		4, 5, 6,
		4, 6, 7,
	}
	return nora.NewGeometry(8, vertices, indices, gl.TRIANGLES, []string{"position", "color"}, nora.InterleavedBuffer)
}
