package main

import (
	"github.com/maja42/gl"
	"github.com/maja42/logicat/geom"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/nora/builtin/shader"
	"github.com/maja42/nora/color"
	"github.com/maja42/vmath"
)

var (
	outerColor     = color.Gray(0.7)
	innerColor     = color.Gray(0.6)
	borderColor    = color.Gray(0.4)
	schematicColor = color.MustHex("262626")
)

var (
	// The schematic consists of cells, the smallest possible unit of placement.
	cellSize = float32(2) // cell size in worldspace

	// The schematic outline displays 3 grids
	// subGrid:	around every cell
	subGridThickness = float32(0.05)
	subGridColor     = color.MustHex("343434")
	// halfGrid:	around every 5 cells
	halfGridSize      = int(5)
	halfGridThickness = float32(0.1)
	halfGridColor     = color.MustHex("343434")
	// mainGrid:	around every 10 cells
	mainGridSize      = int(10)
	mainGridThickness = float32(0.1)
	mainGridColor     = color.MustHex("161616")

	// Additionally, the schematic is surrounded by an outline, which is contained in two borders (outer and inner)
	outlineWidth   = float32(1)
	outerThickness = float32(0.15)
	innerThickness = float32(0.1)
)

type SchematicOutline struct {
	mesh nora.Mesh

	gridSize  vmath.Vec2i // The number of valid positions (corners between cells)
	cellCount vmath.Vec2i // The number of cells (one less than gridSize)

	snapPointOrigin vmath.Vec2f // Position of snapPoint [0,0]
}

func NewSchematicOutline(cellCount vmath.Vec2i) *SchematicOutline {
	mat := nora.NewMaterial(shader.RGB_2D)

	s := &SchematicOutline{
		mesh: *nora.NewMesh(mat),
	}
	s.Resize(cellCount)
	return s
}

// Resize changes the size of the schematic outline.
func (s *SchematicOutline) Resize(cellCount vmath.Vec2i) {
	s.gridSize = cellCount.AddScalar(1)
	s.cellCount = cellCount
	s.snapPointOrigin = s.cellCount.MulScalarf(-cellSize * 0.5) // origin = bottom left

	{
		// Assert snapPos -> world:
		origin := s.WorldCoord(vmath.Vec2i{0, 0})
		max := s.WorldCoord(cellCount)
		assert.True(origin.Equal(max.MulScalar(-1)), "Origin and maximum must be equally far apart of worldspace-zero; origin: %v, max: %v", origin, max)
		// Assert world -> snapPos:
		snap, inside := s.SchemaPos(origin)
		assert.True(inside, "Origin should still be inside schematic")
		assert.True(snap.IsZero(), "Origin should snap to 0x0")
		snap, inside = s.SchemaPos(max)
		assert.True(inside, "Maximum should still be inside schematic")
		assert.True(snap.Equal(cellCount), "Maximum should snap to gridSize-1")
	}
	s.createMesh()
}

// CellCount returns the number of cells in the grid.
// This is one less than GridSize().
func (s *SchematicOutline) CellCount() vmath.Vec2i {
	return s.cellCount
}

// GridSize returns the number of positions on the schematic where things can be placed.
// These are the positions between cells, and is therefore one bigger than CellCount().
func (s *SchematicOutline) GridSize() vmath.Vec2i {
	return s.gridSize
}

// WorldCoord returns the position (in worldspace) of a specific schema position.
func (s *SchematicOutline) WorldCoord(cell vmath.Vec2i) vmath.Vec2f {
	ws := cell.MulScalarf(cellSize)
	return s.snapPointOrigin.Add(ws)
}

// SchemaCoord converts worldspace coordinates into schema coordinates.
// Returns false if this coordinate is outside the schematic.
func (s *SchematicOutline) SchemaCoord(worldPos vmath.Vec2f) (vmath.Vec2f, bool) {
	worldPos = worldPos.Sub(s.snapPointOrigin)
	coord := worldPos.DivScalar(cellSize)
	inside := coord[0] >= 0 && coord[0] < float32(s.gridSize[0]) &&
		coord[1] >= 0 && coord[1] < float32(s.gridSize[1])
	return coord, inside
}

// SchemaPos returns the schema snap position nearest to the given world coordinate.
// Returns false if this snap position is outside the schematic.
func (s *SchematicOutline) SchemaPos(worldPos vmath.Vec2f) (vmath.Vec2i, bool) {
	coord, _ := s.SchemaCoord(worldPos)
	snap := coord.Round()
	inside := snap[0] >= 0 && snap[0] < s.gridSize[0] &&
		snap[1] >= 0 && snap[1] < s.gridSize[1]
	return snap, inside
}

func (s *SchematicOutline) createMesh() {
	var geometry nora.Geometry

	origin := s.WorldCoord(vmath.Vec2i{0, 0})
	max := s.WorldCoord(s.cellCount)

	// background
	background := vmath.RectfFromCorners(
		origin.SubScalar(outlineWidth/2),
		max.AddScalar(outlineWidth/2),
	)
	geometry.AppendGeometry(geom.BorderedRect(background, outlineWidth, schematicColor, borderColor))

	// inner outline border
	border := vmath.RectfFromCorners(origin, max)
	geometry.AppendGeometry(geom.Border(border, innerThickness, innerColor))
	// outer outline border
	border.Min = border.Min.SubScalar(outlineWidth)
	border.Max = border.Max.AddScalar(outlineWidth)
	geometry.AppendGeometry(geom.Border(border, outerThickness, outerColor))
	// grid
	geometry.AppendGeometry(s.grid())

	// todo: border letters

	s.mesh.SetGeometry(&geometry)
}

func (s *SchematicOutline) grid() *nora.Geometry {
	lines := s.gridSize[0] + s.gridSize[1] - 4 // don't draw the outer lines

	vertexCount := lines * 4
	indexCount := lines * 6

	vertices := make([]float32, vertexCount*5)
	indices := make([]uint16, indexCount)

	innerThicknessHalf := innerThickness / 2

	origin := s.WorldCoord(vmath.Vec2i{0, 0})
	max := s.WorldCoord(s.cellCount)

	/* vertical
		 3--2
	     |  |
	     |  |
	     0--1
	*/
	vIdx := uint16(0)
	iIdx := 0
	for i := 1; i < s.gridSize[0]-1; i++ {
		x := origin[0] + float32(i)*cellSize

		thickness := subGridThickness
		color := subGridColor
		if i%mainGridSize == 0 {
			thickness = mainGridThickness
			color = mainGridColor
		} else if i%halfGridSize == 0 {
			thickness = halfGridThickness
			color = halfGridColor
		}

		copy(vertices[vIdx*5:], []float32{
			x - thickness, origin[1] + innerThicknessHalf, color.R, color.G, color.B,
			x + thickness, origin[1] + innerThicknessHalf, color.R, color.G, color.B,
			x + thickness, max[1] - innerThicknessHalf, color.R, color.G, color.B,
			x - thickness, max[1] - innerThicknessHalf, color.R, color.G, color.B,
		})
		copy(indices[iIdx:], []uint16{
			vIdx + 0, vIdx + 1, vIdx + 3, vIdx + 1, vIdx + 2, vIdx + 3,
		})
		vIdx += 4
		iIdx += 6
	}

	/* horizontal
		 3------2
	     |      |
	     0------1
	*/
	for i := 1; i < s.gridSize[1]-1; i++ {
		y := origin[1] + float32(i)*cellSize

		thickness := subGridThickness
		color := subGridColor
		if i%mainGridSize == 0 {
			thickness = mainGridThickness
			color = mainGridColor
		} else if i%halfGridSize == 0 {
			thickness = halfGridThickness
			color = halfGridColor
		}

		copy(vertices[vIdx*5:], []float32{
			origin[0] + innerThicknessHalf, y - thickness, color.R, color.G, color.B,
			max[0] - innerThicknessHalf, y - thickness, color.R, color.G, color.B,
			max[0] - innerThicknessHalf, y + thickness, color.R, color.G, color.B,
			origin[0] + innerThicknessHalf, y + thickness, color.R, color.G, color.B,
		})
		copy(indices[iIdx:], []uint16{
			vIdx + 0, vIdx + 1, vIdx + 3, vIdx + 1, vIdx + 2, vIdx + 3,
		})
		vIdx += 4
		iIdx += 6
	}
	assert.True(int(vIdx) == vertexCount, "Vertex calculation")
	assert.True(iIdx == indexCount, "Index calculation")

	return nora.NewGeometry(vertexCount, vertices, indices, gl.TRIANGLES, []string{"position", "color"}, nora.InterleavedBuffer)
}

func (m *SchematicOutline) Destroy() {
	m.mesh.Destroy()
}

func (m *SchematicOutline) Draw(renderState *nora.RenderState) {
	m.mesh.Draw(renderState)
}
