package action

import (
	"github.com/maja42/glfw"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
)

type Clickable struct {
	panel *Panel

	rect     vmath.Rectf
	button   glfw.MouseButton
	callback OnMouseClickFunc
}

// Bounds returns the clickable's area.
func (e *Clickable) Bounds() vmath.Rectf {
	return e.rect
}

// Destroy removes and therefore disables this clickable element.
func (c *Clickable) Destroy() {
	assert.True(c.panel != nil, "clickable was already disabled")
	c.panel.tree.Remove(c, nil)
	c.panel = nil
}

// Move changes the clickable's area.
func (c *Clickable) Move(area vmath.Rectf) {
	c.panel.tree.Remove(c, nil)
	c.rect = area
	c.panel.tree.Insert(c)
}
