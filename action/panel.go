package action

import (
	"github.com/maja42/glfw"
	"github.com/maja42/nora"
	"github.com/maja42/rtree"
	"github.com/maja42/vmath"
)

type ClipSpaceToModelSpaceFunc func(clipSpacePos vmath.Vec2f) vmath.Vec2f

type OnMouseClickFunc func(mods glfw.ModifierKey)

type Panel struct {
	interactionSys *nora.InteractionSystem
	clipToModel    ClipSpaceToModelSpaceFunc

	mouseButtonCBID nora.CallbackID

	tree *rtree.RTree
}

func NewPanel(interactionSys *nora.InteractionSystem, clipToModel ClipSpaceToModelSpaceFunc) *Panel {
	panel := &Panel{
		interactionSys: interactionSys,
		clipToModel:    clipToModel,

		tree: rtree.New(),
	}

	panel.mouseButtonCBID = interactionSys.OnMouseButtonEvent(panel.onMouseButtonEvent)

	return panel
}

func (t *Panel) onMouseButtonEvent(button glfw.MouseButton, action glfw.Action, mods glfw.ModifierKey) {
	if action != glfw.Release {
		return
	}
	clipSpace := t.interactionSys.MousePosClipSpace()
	pos := t.clipToModel(clipSpace)

	entry := t.getClickable(pos)
	if entry == nil {
		return
	}
	if entry.button != button {
		return
	}
	entry.callback(mods)
}

func (t *Panel) getClickable(pos vmath.Vec2f) *Clickable {
	items := t.tree.SearchPosN(pos, 1)
	if len(items) == 0 {
		return nil
	}
	return items[0].(*Clickable)
}

func (t *Panel) NewClickable(area vmath.Rectf, button glfw.MouseButton, cb OnMouseClickFunc) *Clickable {
	clickable := &Clickable{
		panel:    t,
		rect:     area,
		button:   button,
		callback: cb,
	}
	t.tree.Insert(clickable)
	return clickable
}

func (t *Panel) Destroy() {
	t.interactionSys.RemoveMouseButtonEventFunc(t.mouseButtonCBID)
	t.tree = nil
}
