package main

import (
	"github.com/maja42/logicat/actor"
	"github.com/maja42/logicat/utils"
	"github.com/maja42/nora"
	"github.com/maja42/nora/assert"
	"github.com/maja42/vmath"
	"github.com/maja42/vmath/math32"
	"github.com/maja42/vmath/mathi"
	"github.com/sirupsen/logrus"
)

type SegmentMover struct {
	networking *Networking
	schematic  *Schematic

	configured  bool
	network     *actor.Network // From which network the segments were stolen
	constraints moveConstraints

	original []utils.LineSegment2i // Original line segments that are modified

	previewSize int
	preview     [3]WirePreview
}

func NewSegmentMovement(schematic *Schematic) *SegmentMover {
	a := &SegmentMover{
		networking: schematic.Networking,
		schematic:  schematic,

		// (a)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 50},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	//forceSegVec1: true,
		//	midVec: vmath.Vec2i{1, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{60, 40},
		//		Vec:   vmath.Vec2i{-1, 0},
		//	},
		//	//forceSegVec2: true,
		//},
		// (i)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 40},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	//forceSegVec1: true,
		//	midVec: vmath.Vec2i{1, 1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{60, 50},
		//		Vec:   vmath.Vec2i{-1, 0},
		//	},
		//	forceSegVec2: true,
		//},
		// (b)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	//forceSegVec1: true,
		//	midVec: vmath.Vec2i{0, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{60, 50},
		//		Vec:   vmath.Vec2i{-1, 0},
		//	},
		//	forceSegVec2: true,
		//},
		// (c1)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	forceSegVec1: true,
		//	midVec:       vmath.Vec2i{0, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{50, 40},
		//		Vec:   vmath.Vec2i{-1, 0},
		//	},
		//	forceSegVec2: true,
		//},
		// (c2)
		//constraints: moveConstraints{
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	//forceSegVec1: true,
		//	midVec: vmath.Vec2i{0, 1},
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{50, 40},
		//		Vec:   vmath.Vec2i{-1, 0},
		//	},
		//	forceSegVec1: true,
		//},
		// (d)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec1: true,
		//	midVec:       vmath.Vec2i{1, 0},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{60, 50},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec2: true,
		//},
		// (e)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	forceSegVec1: true,
		//	midVec:       vmath.Vec2i{1, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{55, 70},
		//		Vec:   vmath.Vec2i{0, -1},
		//	},
		//	forceSegVec2: true,
		//},
		// (f)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	//	forceSegVec1: true,
		//	midVec: vmath.Vec2i{1, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{55, 50},
		//		Vec:   vmath.Vec2i{1, 0},
		//	},
		//	forceSegVec2: true,
		//},
		//(g)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec1: true,
		//	midVec:       vmath.Vec2i{1, 1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{60, 70},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec2: true,
		//},

		//(h1)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec1: true,
		//	midVec:       vmath.Vec2i{-1, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{40, 45},
		//		Vec:   vmath.Vec2i{0, 1},
		//	},
		//	forceSegVec2: true,
		//},
		//(h2)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 45},
		//		Vec:   vmath.Vec2i{0, 1},
		//	},
		//	forceSegVec1: true,
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec2: true,
		//	midVec:       vmath.Vec2i{1, 1},
		//},
		////(j)
		//constraints: moveConstraints{
		//	line1: utils.Line2i{
		//		Point: vmath.Vec2i{40, 60},
		//		Vec:   vmath.Vec2i{1, -1},
		//	},
		//	forceSegVec1: true,
		//	midVec:       vmath.Vec2i{0, -1},
		//	line2: utils.Line2i{
		//		Point: vmath.Vec2i{40, 41},
		//		Vec:   vmath.Vec2i{1, 1},
		//	},
		//	forceSegVec2: true,
		//},
		//(k)
		constraints: moveConstraints{
			line1: utils.Line2i{
				Point: vmath.Vec2i{45, 40},
				Vec:   vmath.Vec2i{0, 0},
			},
			forceSegVec1: false,
			midVec:       vmath.Vec2i{0, 5},
			line2: utils.Line2i{
				Point: vmath.Vec2i{40, 50},
				Vec:   vmath.Vec2i{5, -5},
			},
			forceSegVec2: true,
		},
	}
	//a.configured = true

	for i := 0; i < len(a.preview); i++ {
		a.preview[i].Wire = actor.NewWire()
	}

	// markerOverlay.MarkPositions("fixpoint", color.Red, a.constraints.line1.Point, a.constraints.line2.Point)

	return a
}

func (a *SegmentMover) Configure(info *actor.StolenSegment) {
	assert.False(a.configured, "already configured")
	a.configured = true
	segment := info.Segment

	a.original = make([]utils.LineSegment2i, 1, 3)
	a.original[0] = segment

	start := info.StartNeighbour
	if !start.HasZeroLength() {
		a.original = append(a.original, start)
		if start.End != segment.Start {
			start.Start, start.End = start.End, start.Start // Flip direction
			assert.True(start.End == segment.Start, "start neighbour is not a neighbour")
		}
	} else {
		start.Start, start.End = segment.Start, segment.Start
	}

	end := info.EndNeighbour
	if !end.HasZeroLength() {
		a.original = append(a.original, end)
		if segment.End != end.End {
			end.Start, end.End = end.End, end.Start // Flip direction
			assert.True(end.End == segment.End, "end neighbour is not a neighbour")
		}
	} else {
		end.Start, end.End = segment.End, segment.End
	}

	a.network = info.Network
	a.constraints = moveConstraints{
		line1: utils.Line2i{
			Point: start.Start,
			Vec:   start.Vec(),
		},
		forceSegVec1: !start.HasZeroLength(),

		midVec: info.Segment.Vec(),

		line2: utils.Line2i{
			Point: end.Start,
			Vec:   end.Vec(),
		},
		forceSegVec2: !end.HasZeroLength(),
	}
}

func (a *SegmentMover) Destroy() {
	a.Abort()
	a.schematic.MakerOverlay.RemoveBonds()
	for i := 0; i < len(a.preview); i++ {
		a.preview[i].Wire.Destroy()
	}
}

type moveConstraints struct {
	//  <(start)-line1-(end)>---<(start)-segment-(end)>---<(end)-line2-(start)>

	line1        utils.Line2i // first fixed point + vector pointing towards the middle segment
	forceSegVec1 bool         // if segVec1 is valid and should be enforced

	midVec vmath.Vec2i // leads from p1 to p2

	line2        utils.Line2i // first fixed point + vector pointing towards the middle segment
	forceSegVec2 bool         // if segVec2 is valid and should be enforced
}

func determineSegmentVec(fixPoint, nextVec, middle vmath.Vec2i) vmath.Vec2i {
	v := middle.Sub(fixPoint)
	if nextVec[0] == 0 { // vertical -> result is diagonal
		return vmath.Vec2i{signTo1(v[0]), signTo1(nextVec[1])}
	}
	if nextVec[1] == 0 { // horizontal -> result is diagonal
		return vmath.Vec2i{signTo1(nextVec[0]), signTo1(v[1])}
	}
	// diagonal --> result is orthogonal
	// We determine the direction by checking if the distance to the middle point has a higher X or higher Y and go in that direction.
	// This works as long as the middle point is between the two fix-points.
	// If the angle between the point-vector (v) and the nextVec is > 90° (they are pointing in different directions), the result is inverted.
	// In that case, we flip X and Y of the point-vector to change the outcome of the "is X or Y higher" test.

	dot := v.Dot(nextVec)
	if dot == 0 {
		// 90° angle between the point-vector and nextVec --> resolve ambiguity by moving the middle position away.
		v = v.Add(nextVec)
		dot = v.Dot(nextVec)
		assert.True(dot != 0, "failure while calculating segment vector")
	}
	if dot < 0 {
		v[0], v[1] = v[1], v[0]
	}
	if mathi.Abs(v[0]) >= mathi.Abs(v[1]) { // result is horizontal
		return vmath.Vec2i{nextVec[0], 0}
	} else { // result is vertical
		return vmath.Vec2i{0, nextVec[1]}
	}
}

func signTo1(v int) int {
	if v < 0 {
		return -1
	}
	return 1
}

func (c *moveConstraints) Resolve(pos vmath.Vec2i) []utils.LineSegment2i {
	//MarkerOverlay.RemoveMarkers("seg1")
	//MarkerOverlay.RemoveMarkers("seg2")

	if !c.forceSegVec1 { // determine line 1 vector
		c.line1.Vec = determineSegmentVec(c.line1.Point, c.midVec, pos)
	}
	if !c.forceSegVec2 { // determine line 2 vector
		c.line2.Vec = determineSegmentVec(c.line2.Point, c.midVec.Negate(), pos)
	}

	midLine := utils.Line2i{
		Point: pos,
		Vec:   c.midVec,
	}

	if !c.midVec.IsOrthogonal() {
		// When calculating diagonal intersections, the intersection point can be between two cells.
		// If this happens, we move the diagonal line by one cell, which guarantees that the intersection point is now on a cell border.

		isec1, ok1 := c.line1.Intersect(midLine)
		isec2, ok2 := c.line2.Intersect(midLine)
		assert.True(ok1 && ok2, "should have intersected")

		if isec1.Vec2i().Vec2f() != isec1 || isec2.Vec2i().Vec2f() != isec2 {
			pos[1]++ // TODO: use the mouse's float schema Pos to move the midline towards the mouse!
			midLine.Point = pos
		}
	}

	s1, s1ok := resolveSegment(c.line1, midLine)
	midLine.Vec = midLine.Vec.Negate()
	s2, s2ok := resolveSegment(c.line2, midLine)
	midLine.Vec = midLine.Vec.Negate()

	//MarkerOverlay.MarkLineSegment2i("seg1", color.Red, s1)
	//MarkerOverlay.MarkLineSegment2i("seg2", color.Green, s2)

	if !s1ok && !s2ok {
		// None of the 2 segments work (they are both inverted).
		// Set the segment that is wronger (=longer) to zero-length and re-calculate the correct solution.
		// This can be achieved by setting the other segment to "ok", manipulating the "if !sNok" conditions below.
		if s1.SquareLength() > s2.SquareLength() {
			s2ok = true
		} else {
			s1ok = true
		}
	}

	if !s1ok {
		// The calculations state that the middle segment starts at "s1.End", but the resulting path is invalid (s1 is negated).
		// To fix this, we move the middle segment according to the error of s1, which eliminates the needed of segment 1.
		// Afterwards, we re-calculate the path by intersecting the middle segment with segment 2.
		midLine := utils.Line2i{
			Point: s1.Start, // fixed starting point
			Vec:   c.midVec,
		}
		isec, ok := c.line2.Intersect(midLine)
		assert.True(ok, "failed to move segment; unable to find a solution (constraint of segment 1 is always violated)")

		if isec.Vec2i().Vec2f() != isec {
			// The intersection point is between two cells --> Can't eliminate s1 completely.
			// Let s1 have length 1, and re-calculate the intersection point.
			s1.End = s1.Start.Add(c.line1.Vec.Vec2f().Normalize().Vec2i())
			midLine.Point = s1.End
			isec, _ = c.line2.Intersect(midLine)
		} else {
			s1.End = s1.Start // segment 1 does not exist anymore
		}

		s2.End = isec.Vec2i()
		s2ok = true // segment 2 was fixed
	}
	if !s2ok {
		// The calculations state that the middle segment starts at "s1.End", but the resulting path is invalid (s2 is negated).
		// To fix this, we move the middle segment according to the error of s2, which eliminates the needed of segment 2.
		// Afterwards, we re-calculate the path by intersecting the middle segment with segment 1.
		midLine := utils.Line2i{
			Point: s2.Start, // fixed starting point
			Vec:   c.midVec,
		}
		isec, ok := c.line1.Intersect(midLine)
		assert.True(ok, "failed to move segment; unable to find a solution (constraint of segment 2 is always violated)")

		if isec.Vec2i().Vec2f() != isec {
			// The intersection point is between two cells --> Can't eliminate s2 completely.
			// Let s2 have length 1, and re-calculate the intersection point.
			s2.End = s2.Start.Add(c.line2.Vec.Vec2f().Normalize().Vec2i())
			midLine.Point = s2.End
			midLine.Vec = midLine.Vec.Negate()
			isec, _ = c.line1.Intersect(midLine)
		} else {
			s2.End = s2.Start // segment 2 does not exist anymore
		}

		s1.End = isec.Vec2i()
		s1ok = true // segment 1 was fixed
	}

	mid := utils.LineSegment2i{
		Start: s1.End,
		End:   s2.End,
	}

	ignoreNonCollinearMidVector := false
	if !mid.HasZeroLength() {
		if !mid.Vec().IsCollinear(c.midVec) {
			// There is no solution that uses the midVector without negating it.
			// Remove the mid-vector (length=0) and re-calculate the path by intersecting the two segments s1 and s2.

			fisec, ok := c.line1.Intersect(c.line2)
			assert.True(ok, "failed to move segment; unable to find a solution (constraint of middle segment is always violated)")
			isec := fisec.Vec2i()

			if isec.Vec2f() != fisec {
				// The intersection point is between two cells --> Can't eliminate middle vector completely.
				// Let the middle vector have length 1, and move both s1 and s2 half a (diagonal) cell back.
				s1.End = fisec.Sub(s1.Vec().Vec2f().Normalize().MulScalar(diagonalCellLength / 2)).Vec2i()
				s2.End = fisec.Sub(s2.Vec().Vec2f().Normalize().MulScalar(diagonalCellLength / 2)).Vec2i()
				mid.Start, mid.End = s1.End, s2.End // length 1
				assert.True(mid.SquareLength() == 1, "middle segment should have length 1")
				// Note: By moving both cells back, the middle-vector might not be collinear to the original vector anymore.
				// We ignore this fact for simplicity reasons, and because the user won't even recognize it.
				ignoreNonCollinearMidVector = true
			} else {
				s1.End, s2.End = isec, isec
				mid.Start, mid.End = isec, isec // middle segment does not exist anymore
			}
		}
	}

	assert.True(s1.End == mid.Start && mid.End == s2.End, "solution contains disconnected segments")

	segments := make([]utils.LineSegment2i, 0, 3)
	if !s1.HasZeroLength() {
		assert.True(s1.Vec().IsCollinear(c.line1.Vec), "solution for segment 1 is not collinear to required vector")
		segments = append(segments, s1)
	}
	if !mid.HasZeroLength() {
		assert.True(mid.Vec().IsCollinear(c.midVec) || ignoreNonCollinearMidVector, "solution for middle segment 1 is not collinear to required vector")
		segments = append(segments, mid)
	}
	if !s2.HasZeroLength() {
		assert.True(s2.Vec().IsCollinear(c.line2.Vec), "solution for segment 2 is not collinear to required vector")
		segments = append(segments, s2)
	}
	return segments
}

var diagonalCellLength = math32.Sqrt(2)

// resolveSegment returns the required line segment by intersecting it with the stopLine (=middle segment).
// Returns false if the segment has zero Length. Returns false if the calculated segment is not collinear to the requested vector.
func resolveSegment(searchLine, stopLine utils.Line2i) (utils.LineSegment2i /*hasSegment*/, bool) {
	fisec, ok := searchLine.Intersect(stopLine)
	assert.True(ok, "should have intersected")
	isec := fisec.Vec2i()

	assert.True(isec.Vec2f().Equal(fisec), "should not intersect between cells (already resolved by calling function)")

	seg := utils.LineSegment2i{
		Start: searchLine.Point,
		End:   isec,
	}
	if !seg.Vec().IsCollinear(searchLine.Vec) {
		return seg, false
	}
	return seg, true
}

func (a *SegmentMover) Update(mouse vmath.Vec2i) {
	if !a.configured {
		return
	}
	segments := a.constraints.Resolve(mouse)

	a.previewSize = len(segments)

	var good, bad []vmath.Vec2i
	var previousSegBonds *Bonds
	for i, seg := range segments {
		preview := &a.preview[i]
		preview.Segment = seg
		preview.Bonds = CalculateBonds(a.networking, previousSegBonds, seg)
		preview.Wire.ClearPoints()
		preview.Wire.AddPoints(seg.Start, seg.End)

		g, b := preview.Bonds.Positions()
		good = append(good, g...)
		bad = append(bad, b...)
		previousSegBonds = preview.Bonds
	}
	a.showBonds(good, bad)
}

func (a *SegmentMover) showBonds(good, bad []vmath.Vec2i) {
	a.schematic.MakerOverlay.ShowBonds(good, bad)
}

func (a *SegmentMover) Abort() {
	if !a.configured {
		return
	}
	logrus.Infof("Aborting segment movement (restoring %d segments)", len(a.original))

	// Restore original segments
	for _, seg := range a.original {
		a.network.AddWireSegment(seg)
	}
	a.configured = false
	a.previewSize = 0
	a.schematic.MakerOverlay.RemoveBonds()
}

func (a *SegmentMover) Release() {
	if !a.configured {
		logrus.Debugf("Cannot submit movement: nothing configured")
		return
	}
	for i := 0; i < a.previewSize; i++ {
		if a.preview[i].Bonds.HasConflict() {
			logrus.Warn("Cannot submit movement (conflict)")
			return
		}
	}
	logrus.Infof("Submitting segment movement (%d segments)", a.previewSize)

	var net = a.network
	var mergeList map[*actor.Network]vmath.Vec2i

	for i := 0; i < a.previewSize; i++ {
		preview := &a.preview[i]
		if net != nil {
			preview.Bonds.ApplyVia(preview.Segment.Start, net, mergeList)
		}
		net = a.networking.AddWireSegment(preview.Segment, preview.Bonds)
		mergeList = preview.Bonds.Networks // List of networks that were merged
	}
	a.configured = false
	a.previewSize = 0
	a.schematic.MakerOverlay.RemoveBonds()
}

func (a *SegmentMover) Draw(renderState *nora.RenderState) {
	for i := 0; i < a.previewSize; i++ {
		a.preview[i].Wire.Draw(renderState)
	}
}
