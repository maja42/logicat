package theme

import (
	"github.com/maja42/nora"
	"github.com/maja42/nora/builtin/geometry/geo2d"
	"github.com/maja42/nora/color"
)

var (
	BodyColor        = color.MustHex("F2D06B").WithSaturation(0.6)
	BodyBorderColor  = BodyColor.WithBrightness(0.5)
	BodyBorderWidth  = float32(0.15)
	GateLabelFont    *nora.Font // initialized by "Game"
	GateLabelColor   = color.Black
	GateLabelMaxSize = float32(2.5)

	LampValueTextFont *nora.Font // initialized by "Game"

	PinColorInput  = color.Red
	PinColorOutput = color.Magenta
	PinWidth       = float32(0.2)
	PinLength      = float32(0.75)

	PinNotSymbolOuterRadius = float32(0.3)
	PinNotSymbolInnerRadius = PinNotSymbolOuterRadius - BodyBorderWidth

	BondRadius    = float32(0.33) // bonds are dots that mark connected wires
	PinBondRadius = float32(0.25) // bond-dot for connected pins

	OuterHookRadius = BondRadius       // marker size that highlights wire bonds that will be created
	InnerHookRadius = BondRadius * 0.8 // marker size that highlights wire bonds that will be created
	GoodHookColor   = color.Green
	BadHookColor    = color.Red

	WireWidth          = float32(0.2)
	WireLineJoint      = geo2d.BevelJoint
	WireLineCap        = geo2d.RoundLineCap(16)
	WirePassiveColor   = color.Gray(0.95)
	WireInactiveColor  = color.Blue
	WireActiveColor    = color.Green
	WirePlacementColor = color.Gray(0.5)
)
