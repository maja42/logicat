package main

import (
	"github.com/maja42/glfw"
	"github.com/maja42/logicat/dbgpanel"
	"github.com/maja42/nora"
)

type GamePanel struct {
	dbgpanel.DebugPanel
	engine *nora.Engine
	game   *Game

	keyCBID nora.CallbackID
}

func NewGamePanel(engine *nora.Engine, font *nora.Font, game *Game) *GamePanel {
	d := &GamePanel{
		engine: engine,
		game:   game,
	}
	d.DebugPanel = *dbgpanel.NewDebugPanel(font, dbgpanel.TopRightCorner, 13)

	d.keyCBID = engine.InteractionSystem.OnKey(glfw.KeyF2, glfw.Press, d.onF2)
	return d
}

func (d *GamePanel) onF2(glfw.ModifierKey) {
	d.DebugPanel.ToggleVisibility()
}

func (d *GamePanel) Update() {
	panel := &d.DebugPanel

	panel.Section("game").
		Setf("state", d.game.state.String())
}

func (d *GamePanel) Destroy() {
	d.engine.InteractionSystem.RemoveKeyEventFunc(d.keyCBID)
	d.DebugPanel.Destroy()
}

func (m *GamePanel) Draw(renderState *nora.RenderState) {
	m.DebugPanel.Draw(m.engine.Camera.(*nora.OrthoCamera), renderState)
}
